#include "../test_utils.h"
#include "../../src/utils.h"
#include "../../src/simulations/SimulationUtils.h"

using namespace std;

TEST_CASE("Simulation utils tests") {

    SECTION("Filter states 1") {
        const uint N = 5;
        auto numberToStates = [N](const uint x) { return State(N, x); };

        vector<State> states;
        for (uint i = 0; i < 32; ++i) {
            states.push_back(State(N, i));
        };
        vector<State> expected = Utils::applyTo<State>(
                vector<uint>{8, 9, 10, 11, 12, 13, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31},
                numberToStates);
        CHECK(SimulationUtils::getStatesWithOccupiedSite(states, 3) == expected);

        states = Utils::applyTo<State>(vector<uint>{3, 5, 6, 9, 10, 17, 18, 12, 24}, numberToStates);
        expected = Utils::applyTo<State>(vector<uint>{9, 10, 12, 24}, numberToStates);
        CHECK(SimulationUtils::getStatesWithOccupiedSite(states, 3) == expected);
    }
}
