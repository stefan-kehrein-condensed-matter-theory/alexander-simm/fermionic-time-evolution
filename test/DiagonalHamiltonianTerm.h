#ifndef IMPURITY_MODEL_DIAGONALHAMILTONIANTERM_H
#define IMPURITY_MODEL_DIAGONALHAMILTONIANTERM_H

#include "../src/hamiltonians/HamiltonianTerm.h"

class DiagonalHamiltonianTerm : public HamiltonianTerm {
public:
    DiagonalHamiltonianTerm(arma::vec eigenvalues);

    DiagonalHamiltonianTerm(const DiagonalHamiltonianTerm &s);

    DiagonalHamiltonianTerm &operator=(const DiagonalHamiltonianTerm &s);

    ~DiagonalHamiltonianTerm() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    std::string toString() const override;

private:
    arma::vec eigenvalues;
};


#endif //IMPURITY_MODEL_DIAGONALHAMILTONIANTERM_H
