#include "../../src/hamiltonians/InteractionParameter.h"
#include "../test_utils.h"
#include <iostream>
#include "../../src/utils/EntropyCalculator.h"
#include "../../src/utils.h"

using namespace std;
using namespace arma;

TEST_CASE("Entropy calculator tests") {

    /*SECTION("Correct subspace sites") {
        uint N = 10;
        Basis basis(N, N / 2);
        set<uint> sitesA = {0, 1, 2, 3, 4};
        EntropyCalculator ec(basis, sitesA);
        CHECK(ec.getSitesB() == set<uint>{5, 6, 7, 8, 9});

        sitesA = {0, 2, 4, 6, 7, 8, 9};
        ec = EntropyCalculator(basis, sitesA);
        CHECK(ec.getSitesB() == set<uint>{1, 3, 5});

        sitesA = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        ec = EntropyCalculator(basis, sitesA);
        CHECK(ec.getSitesB().empty());
    }

    SECTION("Entropy of pure state density matrix") {
        cx_vec psi = TestUtils::createRandomCplxVector(8, true);
        cx_mat rho = EntropyCalculator::densityMatrix(psi);
        CHECK(EntropyCalculator::entropy(rho, true) == Approx(0.0));
    }

    SECTION("Entropy in trivial case of empty subspace") {
        uint N = 6;
        Basis basis(N, N / 2);
        set<uint> sitesA = {0, 1, 2, 3, 4, 5};
        EntropyCalculator ec(basis, sitesA);

        cx_vec psi = TestUtils::createRandomCplxVector(basis.dim(), true);
        cx_mat rho = ec.partialTrace(psi);

        CHECK(fabs(ec.entropy(rho, true)) == Approx(0.0).margin(1e-8));
    }

    SECTION("Positivity of subspace entropy") {
        for (uint N = 4; N < 8; N++) {
            for (uint Q = 0; Q <= N; Q++) {
                Basis basis(N, Q);
                set<uint> sitesA = Utils::arange(0, N / 2);
                EntropyCalculator ec(basis, sitesA);

                cx_vec psi = TestUtils::createRandomCplxVector(basis.dim(), true);
                cx_mat rho = ec.partialTrace(psi);

                double entropy = ec.entropy(rho, true);
                CHECK((entropy >= 0 || fabs(ec.entropy(rho, true)) == Approx(0.0).margin(1e-8)));
            }
        }
    }*/
}
