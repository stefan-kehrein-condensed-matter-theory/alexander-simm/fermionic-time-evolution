#include "test_utils.h"
#include "../src/simulations/Simulation.h"
#include "simulations/EmptySimulation.h"
#include <armadillo>
#include "../src/utils.h"
#include "../src/HamiltonianBlock.h"
#include "DiagonalHamiltonianTerm.h"

using namespace std;
using namespace arma;

TEST_CASE("Simulation class tests") {

    SECTION("Initialisation") {
        DiagonalHamiltonianTerm diagonal(randu(10));
        HamiltonianBlock H(10, 1, {&diagonal});

        EmptySimulation sim(&H);
        CHECK(sim.getPsi().size() == H.getBasis().dim());
        CHECK(approx_equal(sim.getPsi(), cx_vec(sim.getPsi().size(), fill::zeros), "absdiff", 1e-10));
        CHECK(!H.isDiagonalised());
    }

    SECTION("Set pure state") {
        const uint stateIdx = 3;

        DiagonalHamiltonianTerm diagonal(randu(10));
        HamiltonianBlock H(10, 1, {&diagonal});
        EmptySimulation sim(&H);
        sim.setPsi(H.getBasis().getState(stateIdx));

        cx_vec psi = sim.getPsi();
        for (uint i = 0; i < psi.size(); i++) {
            if (i == stateIdx)
                testComplexApprox(psi[i], 1);
            else
                testComplexApprox(psi[i], 0);
        }
    }

    SECTION("Set mixed state") {
        const vector<uint> stateIndices = {3, 4, 7};
        const vector<complex<double>> weights = {0.3, 0.6, 1.1};
        const vector<complex<double>> normalisedWeights = {0.232845, 0.46569, 0.853765};

        DiagonalHamiltonianTerm diagonal(randu(10));
        HamiltonianBlock H(10, 1, {&diagonal});
        EmptySimulation sim(&H);

        vector<State> states = Utils::applyTo<State>(stateIndices,
                                                     [H](uint index) { return H.getBasis().getState(index); });
        sim.setPsi(states, weights);

        cx_vec psi = sim.getPsi();
        for (uint i = 0; i < psi.size(); i++) {
            int idx = Utils::indexOf(stateIndices, i);
            if (idx >= 0)
                testComplexApprox(psi[i], normalisedWeights[idx]);
            else
                testComplexApprox(psi[i], 0);
        }
    }

    //TODO: test for block diagonal Hamiltonian
}
