#include "../../src/hamiltonians/InteractionParameter.h"
#include "../test_utils.h"
#include <iostream>

using namespace std;

TEST_CASE("SYK interaction parameter tests") {

    SECTION("Parameter conditions are fulfilled") {
        for (uint N = 4; N < 16; N++) {
            for (int Jp = 1; Jp < 4; Jp++) {
                InteractionParameter J(N, 0.5 * Jp);

                for (uint i = 0; i < N; i++) {
                    for (uint j = 0; j < N; j++) {
                        for (uint k = 0; k < N; k++) {
                            for (uint l = 0; l < N; l++) {
                                testComplexApprox(J.get(i, j, k, l), conj(J.get(k, l, i, j)));
                                testComplexApprox(J.get(i, j, k, l), -J.get(j, i, k, l));
                                testComplexApprox(J.get(i, j, k, l), -J.get(i, j, l, k));
                                testComplexApprox(J.get(i, j, k, l), conj(J.get(l, k, j, i)));
                            }
                        }
                    }
                }
            }
        }
    }
}
