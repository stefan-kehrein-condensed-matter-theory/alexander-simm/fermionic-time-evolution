#include "../../src/hamiltonians/SYK2.h"
#include "../../src/HamiltonianBlock.h"
#include "../test_utils.h"
#include "../../src/utils.h"

using namespace std;
using namespace arma;

TEST_CASE("SYK2 Hamiltonian term class tests") {

    SECTION("Hamiltonian is hermitian") {
        SYK2 syk(8, 1);
        HamiltonianBlock H(8, 4, {&syk});

        cx_mat m = H.asMatrix();
        for (int i = 0; i < m.n_rows; ++i) {
            CHECK(imag(m(i, i)) < 1e-10);

            for (int j = 0; j < i; ++j) {
                testComplexApprox(m(i, j), conj(m(j, i)));
            }
        }

        CHECK(approx_equal(H.asMatrix().t(), H.asMatrix(), "absdiff", 1e-10));
    }

        //TODO: repair this!
        /*SECTION("Number of non-vanishing entries") {
            const uint N = 4;
            const uint Q = 3;

            InteractionParameter J(N, 1);
            SYKHamiltonianBlock hamiltonian(N, Q, J);

            const uint dim = hamiltonian.dim();
            const cx_mat H = hamiltonian.asMatrix();
            cout << H << endl;
            uint count = 0;
            for (uint i = 0; i < dim; i++) {
                for (uint j = 0; j < dim; j++) {
                    if (abs(H(i, j)) > 1e-10)
                        count++;
                }
            }
            cout << "counted: " << count << endl;

            uint expected = binomial(N, Q); // place Q particle on N sites
            expected *= Q * (Q - 1); // annihilate two particles
            expected *= (N - Q + 2) * (N - Q + 1); // create two particles
            expected /= 4; // lead to same final state
            cout << "expected: " << expected << endl;
            CHECK(count == expected);
        }*/

    SECTION("Real eigenvalues") {
        SYK2 syk(8, 1);
        HamiltonianBlock H(8, 4, {&syk});

        vec evals;
        cx_mat evecs;
        bool success = eig_sym(evals, evecs, H.asMatrix());
        CHECK(success);

        CHECK(approx_equal(imag(evals), vec(evals.size(), fill::zeros), "absdiff", 1e-5));

        H.diagonalise();
        CHECK(approx_equal(H.getEigenvalues(), evals, "absdiff", 1e-5));
    }
}
