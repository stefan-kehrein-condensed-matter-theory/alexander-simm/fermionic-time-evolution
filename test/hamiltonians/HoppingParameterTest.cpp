#include "../../src/hamiltonians/HoppingParameter.h"
#include "../test_utils.h"
#include <iostream>

using namespace std;

TEST_CASE("SYK interaction parameter 2 tests") {

    SECTION("Parameter conditions are fulfilled") {
        for (uint N = 4; N < 16; N++) {
            for (int Jp = 0; Jp < 5; Jp++) {
                HoppingParameter J(N, 0.5 * Jp);

                for (uint i = 0; i < N; i++) {
                    for (uint j = i + 1; j < N; j++) {
                        testComplexApprox(J.get(i, j), conj(J.get(j, i)));
                    }
                }
            }
        }
    }
}
