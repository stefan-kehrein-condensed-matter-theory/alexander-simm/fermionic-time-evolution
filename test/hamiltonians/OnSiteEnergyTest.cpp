#include "../../src/hamiltonians/OnSiteEnergy.h"
#include "../../src/HamiltonianBlock.h"
#include "../test_utils.h"
#include "../../src/utils.h"

using namespace std;
using namespace arma;

TEST_CASE("On-site energy Hamiltonian term class tests") {

    SECTION("Hamiltonian is hermitian") {
        OnSiteEnergy onsite(linspace(-1, 1, 8));
        HamiltonianBlock H(8, 4, {&onsite});

        cx_mat m = H.asMatrix();
        for (int i = 0; i < m.n_rows; ++i) {
            CHECK(imag(m(i, i)) < 1e-10);

            for (int j = 0; j < i; ++j) {
                testComplexApprox(m(i, j), conj(m(j, i)));
            }
        }

        CHECK(approx_equal(H.asMatrix().t(), H.asMatrix(), "absdiff", 1e-10));
    }

        //TODO: repair this!
        /*SECTION("Number of non-vanishing entries") {
            const uint N = 4;
            const uint Q = 3;

            InteractionParameter J(N, 1);
            SYKHamiltonianBlock hamiltonian(N, Q, J);

            const uint dim = hamiltonian.dim();
            const cx_mat H = hamiltonian.asMatrix();
            cout << H << endl;
            uint count = 0;
            for (uint i = 0; i < dim; i++) {
                for (uint j = 0; j < dim; j++) {
                    if (abs(H(i, j)) > 1e-10)
                        count++;
                }
            }
            cout << "counted: " << count << endl;

            uint expected = binomial(N, Q); // place Q particle on N sites
            expected *= Q * (Q - 1); // annihilate two particles
            expected *= (N - Q + 2) * (N - Q + 1); // create two particles
            expected /= 4; // lead to same final state
            cout << "expected: " << expected << endl;
            CHECK(count == expected);
        }*/

    SECTION("Real eigenvalues") {
        OnSiteEnergy onsite(linspace(-1, 1, 8));
        HamiltonianBlock H(8, 4, {&onsite});

        vec evals;
        cx_mat evecs;
        bool success = eig_sym(evals, evecs, H.asMatrix());
        CHECK(success);

        CHECK(approx_equal(imag(evals), vec(evals.size(), fill::zeros), "absdiff", 1e-5));

        H.diagonalise();
        CHECK(approx_equal(H.getEigenvalues(), evals, "absdiff", 1e-5));
    }

    SECTION("Orthonormal eigenvectors") {
        OnSiteEnergy onsite(linspace(1.0, 8.0, 8));
        HamiltonianBlock H(8, 4, {&onsite});
        H.diagonalise();

        for (uint i = 0; i < H.getBasis().dim(); ++i) {
            cx_vec evec = H.getEigenvectors().col(i);
            CHECK(norm(evec) == Approx(1.0));

            for (int j = 0; j < i; ++j) {
                testComplexApprox(cdot(evec, H.getEigenvectors().col(j)), 0);
            }
        }
    }

    SECTION("Matrix structure single particles 1") {
        OnSiteEnergy onsite(Utils::convert<double, vec>(vector<double>{0.0}));
        HamiltonianBlock H(1, 1, {&onsite}, 1, 1);
        cx_mat expected;
        expected << 0 << 1 << endr << 1 << 1;
        testArmaApprox(H.matrix(), expected);
    }

    SECTION("Matrix structure single particles 2") {
        OnSiteEnergy onsite(Utils::convert<double, vec>({-2, -1, 1, 2}));
        HamiltonianBlock H(4, 1, {&onsite}, 0, 1);

        cx_mat expected(5, 5);
        expected << -2 << 0 << 0 << 0 << 0.5 << endr
                 << 0 << -1 << 0 << 0 << 0.5 << endr
                 << 0 << 0 << 1 << 0 << 0.5 << endr
                 << 0 << 0 << 0 << 2 << 0.5 << endr
                 << 0.5 << 0.5 << 0.5 << 0.5 << 0;
        testArmaApprox(H.matrix(), expected);
    }

    SECTION("Matrix structure many particles") {
        OnSiteEnergy onsite(Utils::convert<double, vec>({-2, -1, 2}));
        HamiltonianBlock H(3, 2, {&onsite}, 1, 1);
        CHECK(H.getBasis().dim() == 6);

        cx_mat expected(6, 6);
        double v = 1.0 / sqrt(3.0);
        expected << -3 << 0 << 0 << v << -v << 0 << endr
                 << 0 << 0 << 0 << v << 0 << -v << endr
                 << 0 << 0 << 1 << 0 << v << -v << endr
                 << v << v << 0 << -1 << 0 << 0 << endr
                 << -v << 0 << v << 0 << 0 << 0 << endr
                 << 0 << -v << -v << 0 << 0 << 3;

        testArmaApprox(H.asMatrix(), expected);
    }

    //TODO
    /*SECTION("Eigenvalues and eigenvectors") {
        AndersonHamiltonianBlock H1(1, linspace(-1, 1, 9), 0, 0.25);
        cout << real(H1.matrix()) << endl;
        H1.diagonalise();

            vector<double> evals = {-1.0074417, -0.75967033, -0.51411389, -0.27560626, -0.07204379, 0.07204379,
                                    0.27560626, 0.51411389, 0.75967033, 1.0074417};
            CHECK(norm(H1.getEigenvalues() - Utils::convert<double, vec>(evals)) < 1e-4);

            cx_mat evecs(H1.dim(), H1.dim());
            evecs
                    << 0.99539828 << -0.03989582 << -0.02851843 << 0.033331 << -0.05442271 << -0.04710805 << -0.01892807
                    << -0.0091517 << 0.00544883 << 0.00369 << endr
                    << 0.02877333 << 0.99150152 << -0.05874322 << 0.05089605 << -0.07449138 << -0.06143455 << -0.02354195
                    << -0.0109616 << 0.00635115 << 0.00421491 << endr
                    << 0.01459765 << 0.03692431 << 0.98177811 << 0.1076 << -0.11800715 << -0.08828326 << -0.03113019
                    << -0.01366386 << 0.00761163 << 0.00491392 << endr
                    << 0.00977957 << 0.01881245 << 0.0524649 << -0.9429245 << -0.2837883 << -0.15681685 << -0.04593699
                    << -0.01813435 << 0.00949632 << 0.00589089 << endr
                    << -0.08888945 << -0.11505779 << -0.16628051 << 0.28973721 << -0.6060227 << -0.6060227 << -0.28973721
                    << -0.16628051 << 0.11505779 << 0.08888945 << endr
                    << 0.00735274 << 0.01262146 << 0.02695261 << -0.08760602 << 0.70098886 << -0.70098886 << -0.08760602
                    << -0.02695261 << 0.01262146 << 0.00735274 << endr
                    << 0.00589089 << 0.00949632 << 0.01813435 << -0.04593699 << 0.15681685 << 0.2837883 << -0.9429245
                    << -0.0524649 << 0.01881245 << 0.00977957 << endr
                    << 0.00491392 << 0.00761163 << 0.01366386 << -0.03113019 << 0.08828326 << 0.11800715 << 0.1076
                    << -0.98177811 << 0.03692431 << 0.01459765 << endr
                    << 0.00421491 << 0.00635115 << 0.0109616 << -0.02354195 << 0.06143455 << 0.07449138 << 0.05089605
                    << 0.05874322 << 0.99150152 << 0.02877333 << endr
                    << 0.00369 << 0.00544883 << 0.0091517 << -0.01892807 << 0.04710805 << 0.05442271 << 0.033331
                    << 0.02851843 << -0.03989582 << 0.99539828;

            cout << H1.getEigenvectors() << endl;
            cx_mat ev = H1.getEigenvectors();
            for (uint i = 0; i < H1.dim(); i++) {
                for (uint j = 0; j < H1.dim(); j++) {
                    if ((norm(ev(i, j) - evecs(i, j)) > 1e-3 && norm(ev(i, j) + evecs(i, j)) > 1e-3)) {
                        cout << i << "," << j << ": " << ev(i, j) << "\t" << evecs(i, j) << endl;
                    }
                }
            }
            for (int i = 0; i < H1.dim(); ++i) {
                cx_vec col = H1.getEigenvectors().col(i);
                CHECK((norm(col - evecs.col(i)) < 1e-3 || norm(col + evecs.col(i)) < 1e-3));
            }
        }*/

    /*SECTION("Sparse eigenvalues") {
        vec energies = linspace(1.1984, 7.123, 8);
        AndersonHamiltonianBlock H(4, energies, false);
        CHECK(!H.isUsingSparseMatrix());
        H.diagonalise(true);
        CHECK(H.isDiagonalised());

        AndersonHamiltonianBlock Hsparse(4, energies, true);
        CHECK(Hsparse.isUsingSparseMatrix());
        Hsparse.diagonalise(false);
        CHECK(Hsparse.isDiagonalised());

        testVectorApprox(H.getEigenvalues(), Hsparse.getEigenvalues());
    }

    SECTION("Sparse ground state") {
        vec energies = linspace(1.1984, 7.123, 8);
        AndersonHamiltonianBlock H(4, energies, false);
        CHECK(!H.isUsingSparseMatrix());
        H.diagonalise(true);
        CHECK(H.isDiagonalised());

        AndersonHamiltonianBlock Hsparse(4, energies, true);
        CHECK(Hsparse.isUsingSparseMatrix());
        Hsparse.diagonalise(true);
        CHECK(Hsparse.isDiagonalised());

        testVectorApprox(H.getEigenvectors().col(0), Hsparse.getEigenvectors().col(0));
    }*/
}
