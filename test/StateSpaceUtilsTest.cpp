#include "test_utils.h"
#include "../src/utils/GreensFunctionCalculator.h"
#include "../src/Basis.h"
#include <filesystem>

using namespace std;
using namespace arma;

//TODO: fix this
TEST_CASE("State space utils tests") {

    /*SECTION("Annihilation matrix") {
        // N=2 sites, Q=0,1,2 particles
        uint N = 2;

        Basis H1(N / 2 - 1, 1);
        Basis H2(N / 2, 1);
        Basis H3(N / 2 + 1, 1);

        mat expected0a(2, 1);
        expected0a << 0 << endr << 1 << endr;
        mat *C0a = GreensFunctionCalculator::annihilationMatrix(&H3, &H2, 0);
        CHECK(approx_equal(*C0a, expected0a, "absdiff", 1e-5));
        delete C0a;

        mat expected1a(2, 1);
        expected1a << -1 << endr << 0 << endr;
        mat *C1a = GreensFunctionCalculator::annihilationMatrix(&H3, &H2, 1);
        CHECK(approx_equal(*C1a, expected1a, "absdiff", 1e-5));
        delete C1a;

        mat expected0b(1, 2);
        expected0b << 1 << 0 << endr;
        mat *C0b = GreensFunctionCalculator::annihilationMatrix(&H2, &H1, 0);
        CHECK(approx_equal(*C0b, expected0b, "absdiff", 1e-5));
        delete C0b;

        mat expected1b(1, 2);
        expected1b << 0 << 1 << endr;
        mat *C1b = StateSpaceUtils::annihilationMatrix(&H2, &H1, 1);
        CHECK(approx_equal(*C1b, expected1b, "absdiff", 1e-5));
        delete C1b;
    }*/

    //TODO: test for block diagonal Hamiltonian
}
