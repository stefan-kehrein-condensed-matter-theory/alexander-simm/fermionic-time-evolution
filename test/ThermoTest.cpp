#include "test_utils.h"
#include "../src/Thermo.h"
#include "../src/HamiltonianBlock.h"
#include "DiagonalHamiltonianTerm.h"

using namespace std;
using namespace arma;

TEST_CASE("Thermo tests") {

    SECTION("Free energy") {
        DiagonalHamiltonianTerm diagonal({1.0, 2.0, 3.0, 4.0});
        HamiltonianBlock H(4, 1, {&diagonal});
        H.diagonalise();

        //partition function Z: 0.571317431664653136762583702
        //avg energy E: 1.5073472654142302326293824756
        CHECK(Thermo::freeEnergy(&H, 1) == Approx(0.55981030143880));
        CHECK(Thermo::entropy(&H, 1.0) == Approx(0.94753696397542556312));
        //Z: 1.332875544269118266026603
        //E: 1.9154235115381356768587813
        CHECK(Thermo::freeEnergy(&H, 0.5) == Approx(-0.5746773433966590298));
        CHECK(Thermo::entropy(&H, 0.5) == Approx(1.2450504274673973533293));

        diagonal = DiagonalHamiltonianTerm({-2.0, -1.0, 0.0, 1.0, 2.0});
        HamiltonianBlock H2(5, 1, {&diagonal});
        H2.diagonalise();

        //Z: 11.6105526517977504760
        //E: -1.45194156766219473111
        CHECK(Thermo::freeEnergy(&H2, 1) == Approx(-2.4519143959375933));
        CHECK(Thermo::entropy(&H2, 1) == Approx(0.99997282827539826889));
        //Z: 6.34141320004324912740
        //E: -0.90563336663246178279
        CHECK(Thermo::freeEnergy(&H2, 0.5) == Approx(-3.69420329165030078144));
        CHECK(Thermo::entropy(&H2, 0.5) == Approx(1.394284962508919499325));
    }

    //TODO: test for block diagonal Hamiltonian
}
