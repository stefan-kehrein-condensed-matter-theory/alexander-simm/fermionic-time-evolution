#ifndef STATE_H__
#define STATE_H__

#include <vector>
#include <string>
#include <set>

/**
 * Definition: |1, ..., N> = c*_1 ...c*_N |0>
 */
class State {
public:
    /** Constructs a state with a given state number. */
    State(uint N, ulong stateNumber);

    State(const State &s);

    State &operator=(const State &other);

    bool operator==(const State &other) const;

    bool operator!=(const State &other) const;

    /** Whether this is a zero state (not the vacuum!), i.e. the coefficient is 0. */
    bool isZero() const;

    /** Returns this state's coefficient +1, -1, or 0. */
    int getCoefficient() const;

    /** Returns a list with all site indices that are occupied by the state with the given number and charge. */
    std::vector<uint> getOccupiedSites() const;

    /** Returns whether the site with the given index is occupied by this state. */
    bool isSiteOccupied(uint site) const;

    /** Returns the number of this state in the block combinatorial number system. */
    ulong getStateNumber() const;

    /** Returns the number of particle, i.e. the number of creation operators, in this state. */
    int numParticles() const;

    /** Returns the number of occupied positions in the state between the two positions (both inclusive). */
    uint numOccupiedSites(uint pos1, uint pos2) const;

    /** Returns how many of the given sites are occupied. */
    uint numOccupiedSites(std::set<uint> sites) const;

    /** Returns the number of sites that are occupied differently in both states. */
    uint numDifferentSites(const State &other) const;

    std::string toString() const;

    std::string toBinaryString() const;

    /** Annihilate the particle at site i. If the site is empty the state will be zero afterwards. */
    void annihilate(uint site);

    /** Creates a particle at site i. If the site is occupied the state will be zero afterwards. */
    void create(uint i);

    /**
     * Applies a hopping term: annihilates site j, creates site i, and returns the resulting state.
     */
    State applyTerm(uint i, uint j) const;

    /**
     * Applies a hopping term: annihilates site j, creates site i, and saves the result in the destination
     * state.
     */
    void applyTerm(uint i, uint j, State &dst) const;

    /**
     * Applies an interaction term: annihilates site l and k, creates site j and i, and returns the resulting
     * state.
     */
    State applyTerm(uint i, uint j, uint k, uint l) const;

    /**
     * Applies an interaction term: annihilates site l and k, creates site j and i, and savees the result in
     * the destination state.
     */
    void applyTerm(uint i, uint j, uint k, uint l, State &dst) const;

    static State combineStates(State stateA, std::vector<uint> sitesA, State stateB, std::vector<uint> sitesB);

private:
    uint N;
    ulong bits;
    int coefficient;
};

inline std::ostream &operator<<(std::ostream &out, const State &s) {
    out << s.toString();
    return out;
}

#endif // STATE_H__
