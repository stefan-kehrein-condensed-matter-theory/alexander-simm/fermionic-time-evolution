#ifndef IMPURITY_MODEL_BLOCKDIAGONALHAMILTONIAN_H
#define IMPURITY_MODEL_BLOCKDIAGONALHAMILTONIAN_H

#include "Hamiltonian.h"
#include "HamiltonianBlock.h"

/**
 * Base class for all Hamiltonians that consist of multiple blocks.
 */
class BlockDiagonalHamiltonian : public Hamiltonian {
public:
    /** Constructs a block diagonal Hamiltonian with N sites without impurity. */
    BlockDiagonalHamiltonian(uint N, std::vector<HamiltonianTerm *> terms, bool sparse = false);

    /** Constructs a block diagonal Hamiltonian with N sites plus one impurity. */
    BlockDiagonalHamiltonian(uint N, std::vector<HamiltonianTerm *> terms, double impurityEnergy, double V,
            bool sparse = false);

    BlockDiagonalHamiltonian(const BlockDiagonalHamiltonian &h);

    ~BlockDiagonalHamiltonian();

    BlockDiagonalHamiltonian &operator=(const BlockDiagonalHamiltonian &h);

    const Basis &getBasis() const override;

    bool isUsingSparseMatrix() const override;

    double getSparseness() const override;

    arma::cx_mat asMatrix() const override;

    arma::sp_cx_mat asSparseMatrix() const override;

    bool isDiagonalised() const override;

    void diagonalise(bool includeEigenvectors = true, bool onlyGroundState = false) override;

    const arma::vec &getEigenvalues() const override;

    const arma::cx_mat &getEigenvectors() const override;

    /**
     * Returns all blocks if this Hamiltonian sorted by increasing fermion number from 0 to N.
     *
     * @return a list of blocks
     */
    std::vector<HamiltonianBlock *> getBlocks() const;

    /**
     * Returns the block with the fermion number Q.
     *
     * @param Q number of fermions between 0 and N
     * @return the block or nullptr if Q is out of range
     */
    HamiltonianBlock *getBlock(uint Q) const;

    void updateImpurityEnergy(double E) override;

    void updateHoppingPotential(double V) override;

    void updateMatrix(bool updateTerms = false) override;

private:
    /** Basis of the Fock state. */
    Basis basis;
    /** Subblocks sorted in ascending particle number. */
    std::vector<HamiltonianBlock *> blocks;

    arma::vec eigenvalues;
    arma::cx_mat eigenvectors;
};

#endif //IMPURITY_MODEL_BLOCKDIAGONALHAMILTONIAN_H
