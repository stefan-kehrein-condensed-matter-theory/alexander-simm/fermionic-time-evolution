#include "Thermo.h"
#include "utils.h"

using namespace std;

double Thermo::freeEnergy(const Hamiltonian *hamiltonian, double beta) {
    return -log(hamiltonian->partitionFunction(beta)) / beta;
}

double Thermo::entropy(const Hamiltonian *hamiltonian, double beta) {
    return beta * (hamiltonian->averageEnergy(beta) - freeEnergy(hamiltonian, beta));
}

vector<double> Thermo::entropyDensity(const Hamiltonian *H, std::vector<double> beta) {
    return Utils::applyTo<double>(beta, [&H](double b) {
        return Thermo::entropy(H, b) / H->getBasis().getN();
    });
}
