#ifndef HAMILTONIAN_TERM_H
#define HAMILTONIAN_TERM_H

#include "TermCallback.h"
#include "../Basis.h"
#include <armadillo>
#include <set>

class HamiltonianTerm {
public:
    HamiltonianTerm(bool real);

    HamiltonianTerm(const HamiltonianTerm &h);

    HamiltonianTerm &operator=(const HamiltonianTerm &h);

    virtual ~HamiltonianTerm() = default;

    virtual std::string toString() const = 0;

    /**
     * Whether this term will only contribute real entries to the Hamiltonian.
     */
    bool isReal() const;

    /**
     * Updates this term. Terms that depend on random numbers should override this method and reinitialise
     * those numbers.
     */
    virtual void update();

    /**
     * Adds all entries of this term to the Hamiltonian by calling the callback.
     */
    virtual void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) = 0;

private:
    bool real;
};

#endif //HAMILTONIAN_TERM_H
