#ifndef IMPURITY_MODEL_CHEMICALPOTENTIAL_H
#define IMPURITY_MODEL_CHEMICALPOTENTIAL_H

#include "HamiltonianTerm.h"

class ChemicalPotential : public HamiltonianTerm {
public:
    ChemicalPotential(double mu);

    ChemicalPotential(const ChemicalPotential &s);

    ChemicalPotential &operator=(const ChemicalPotential &s);

    ~ChemicalPotential() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    std::string toString() const override;

    double getMu() const;

    void setMu(double mu);

private:
    double mu;
};

#endif //IMPURITY_MODEL_CHEMICALPOTENTIAL_H
