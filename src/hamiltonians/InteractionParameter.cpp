#include "InteractionParameter.h"
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <chrono>

using namespace std;

InteractionParameter::InteractionParameter(uint N, double J) : N(N), J(J), values(boost::extents[N][N][N][N]) {
    boost::random::mt19937 gen(chrono::high_resolution_clock::now().time_since_epoch().count());
    boost::random::normal_distribution<> dist(0, J);

    for (uint i = 0; i < N; i++) {
        for (uint j = 0; j < i; j++) {
            for (uint k = 0; k < N; k++) {
                for (uint l = 0; l < k; l++) {
                    if (i == k && j == l)
                        values[i][j][k][l] = complex<double>(dist(gen), 0);
                    else
                        values[i][j][k][l] = M_SQRT1_2 * complex<double>(dist(gen), dist(gen));

                    // hermitian and anti-commutation relation
                    values[j][i][k][l] = -values[i][j][k][l];
                    values[i][j][l][k] = -values[i][j][k][l];
                    values[j][i][l][k] = values[i][j][k][l];

                    values[k][l][i][j] = conj(values[i][j][k][l]);
                    values[l][k][i][j] = -values[k][l][i][j];
                    values[k][l][j][i] = -values[k][l][i][j];
                    values[l][k][j][i] = values[k][l][i][j];
                }
            }
        }
    }
}

InteractionParameter::InteractionParameter(const InteractionParameter &p) {
    this->operator=(p);
}

InteractionParameter &InteractionParameter::operator=(const InteractionParameter &p) {
    if (this != &p) {
        N = p.N;
        J = p.J;
        values = p.values;
    }

    return *this;
}

uint InteractionParameter::getN() const {
    return N;
}

double InteractionParameter::getJ() const {
    return J;
}

complex<double> InteractionParameter::get(uint i, uint j, uint k, uint l) const {
    return values[i][j][k][l];
}
