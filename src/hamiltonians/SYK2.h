#ifndef IMPURITY_MODEL_SYK2_H
#define IMPURITY_MODEL_SYK2_H

#include "HamiltonianTerm.h"
#include "HoppingParameter.h"

class SYK2 : public HamiltonianTerm {
public:
    SYK2(uint N, double paramJ);

    SYK2(const SYK2 &s);

    SYK2 &operator=(const SYK2 &s);

    ~SYK2() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    void update() override;

    std::string toString() const override;

private:
    HoppingParameter J;
};

#endif //IMPURITY_MODEL_SYK2_H
