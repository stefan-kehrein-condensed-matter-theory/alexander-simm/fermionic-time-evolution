#ifndef IMPURITY_MODEL_ONSITEENERGY_H
#define IMPURITY_MODEL_ONSITEENERGY_H

#include "HamiltonianTerm.h"

class OnSiteEnergy : public HamiltonianTerm {
public:
    OnSiteEnergy(const arma::vec &energies);

    OnSiteEnergy(const OnSiteEnergy &o);

    OnSiteEnergy &operator=(const OnSiteEnergy &o);

    ~OnSiteEnergy() = default;

    void addEntries(TermCallback *callback, const Basis &basis, std::set<uint> bandSites) override;

    std::string toString() const override;

    const arma::vec &getEnergies() const;

private:
    arma::vec energies;
};

#endif //IMPURITY_MODEL_ONSITEENERGY_H
