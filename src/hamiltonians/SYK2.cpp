#include "SYK2.h"
#include "../Log.h"

using namespace std;

SYK2::SYK2(uint N, double paramJ) : HamiltonianTerm(false), J(N, paramJ) {
}

SYK2::SYK2(const SYK2 &s) : HamiltonianTerm(s), J(s.J) {
    this->operator=(s);
}

SYK2 &SYK2::operator=(const SYK2 &s) {
    if (&s != this) {
        HamiltonianTerm::operator=(s);
        J = s.J;
    }

    return *this;
}

std::string SYK2::toString() const {
    return "SYK q=2 (J=" + to_string(J.getJ()) + ")";
}

void SYK2::update() {
    J = HoppingParameter(J.getN(), J.getJ());
}

void SYK2::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    State state2(basis.getN(), 0);
    double scaling = 1.0 / sqrt(bandSites.size());
    const vector<State> &states = basis.getStates();

    ulong idx = 0;
    for (const State &state1 : states) {
        if (basis.getStates().size() > 100 && basis.getN() > 12 && (idx++) % 100 == 0)
            Log::debug() << "\tstate " << idx << " / " << states.size() << endl;

        ulong state1Idx = basis.getStateIndex(state1);

        for (uint i : bandSites) {
            for (uint j : bandSites) {
                state1.applyTerm(i, j, state2);
                if (!state2.isZero())
                    callback->addEntry(scaling * J.get(i, j), state1, state2, state1Idx);
            }
        }

        // correction terms
        for (uint i : bandSites) {
            callback->addEntry(-0.5 * scaling * J.get(i, i), state1, state1, state1Idx);
        }
    }
}