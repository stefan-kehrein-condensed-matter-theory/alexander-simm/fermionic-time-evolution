#include "HoppingParameter.h"
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <chrono>

using namespace std;

HoppingParameter::HoppingParameter(uint N, double J) : N(N), J(J), values(boost::extents[N][N]) {
    boost::random::mt19937 gen(chrono::high_resolution_clock::now().time_since_epoch().count());
    boost::random::normal_distribution<> dist(0, J);

    for (uint i = 0; i < N; i++) {
        for (uint j = 0; j <= i; j++) {
            if (i == j)
                values[i][j] = complex<double>(dist(gen), 0);
            else {
                values[i][j] = M_SQRT1_2 * complex<double>(dist(gen), dist(gen));
                values[j][i] = conj(values[i][j]); // hermitian
            }
        }
    }
}

HoppingParameter::HoppingParameter(const HoppingParameter &p) {
    this->operator=(p);
}

HoppingParameter &HoppingParameter::operator=(const HoppingParameter &p) {
    if (this != &p) {
        N = p.N;
        J = p.J;
        values = p.values;
    }

    return *this;
}

uint HoppingParameter::getN() const {
    return N;
}

double HoppingParameter::getJ() const {
    return J;
}

complex<double> HoppingParameter::get(uint i, uint j) const {
    return values[i][j];
}

arma::cx_mat HoppingParameter::asMatrix() const {
    arma::cx_mat m(N, N);
    for (uint i = 0; i < N; i++) {
        for (uint j = 0; j < N; j++) {
            m(i, j) = values[i][j];
        }
    }
    return m;
}