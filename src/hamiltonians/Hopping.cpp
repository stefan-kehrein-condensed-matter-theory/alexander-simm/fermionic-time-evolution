#include "Hopping.h"

using namespace std;

Hopping::Hopping(double hoppingEnergy) : HamiltonianTerm(true), hoppingEnergy(hoppingEnergy) {
}

Hopping::Hopping(const Hopping &s) : HamiltonianTerm(s) {
    this->operator=(s);
}

Hopping &Hopping::operator=(const Hopping &h) {
    if (&h != this) {
        HamiltonianTerm::operator=(h);
        hoppingEnergy = h.hoppingEnergy;
    }

    return *this;
}

std::string Hopping::toString() const {
    return "hopping (energy=" + to_string(hoppingEnergy) + ")";
}

void Hopping::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    const uint N = basis.getN();
    State state2(N, 0);

    for (const State &state : basis.getStates()) {
        ulong stateIdx = basis.getStateIndex(state);

        for (uint i : bandSites) {
            for (uint j : bandSites) {
                state.applyTerm(i, j, state2);
                if (!state2.isZero())
                    callback->addEntry(hoppingEnergy / N, state, state2, stateIdx);
            }
        }
    }
}
