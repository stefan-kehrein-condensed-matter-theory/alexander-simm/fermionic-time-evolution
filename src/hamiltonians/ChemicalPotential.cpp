#include "ChemicalPotential.h"

using namespace std;

ChemicalPotential::ChemicalPotential(double mu) : HamiltonianTerm(true), mu(mu) {
}

ChemicalPotential::ChemicalPotential(const ChemicalPotential &s) : HamiltonianTerm(s) {
    this->operator=(s);
}

ChemicalPotential &ChemicalPotential::operator=(const ChemicalPotential &s) {
    if (&s != this) {
        HamiltonianTerm::operator=(s);
        mu = s.mu;
    }

    return *this;
}

std::string ChemicalPotential::toString() const {
    return "chem.pot. (mu=" + to_string(mu) + ")";
}

void ChemicalPotential::addEntries(TermCallback *callback, const Basis &basis, set<uint> bandSites) {
    for (const State &state : basis.getStates()) {
        ulong stateIdx = basis.getStateIndex(state);

        uint num = state.numOccupiedSites(bandSites);
        callback->addEntry(-mu * num, state, state, stateIdx, stateIdx);
    }
}

double ChemicalPotential::getMu() const {
    return mu;
}

void ChemicalPotential::setMu(double mu) {
    this->mu = mu;
}