#include <iostream>
#include <vector>
#include <armadillo>
#include "Thermo.h"
#include "utils.h"
#include "utils/StorageFile.h"
#include "simulations/ExactSimulation.h"
#include "simulations/SimulationUtils.h"
#include "utils/Histogram1D.h"
#include "utils/GreensFunctionCalculator.h"
#include "simulations/LanczosSimulation.h"
#include "utils/Timer.h"
#include "observables/EntropyObservable.h"
#include "observables/OccupationObservable.h"
#include "hamiltonians/OnSiteEnergy.h"
#include "hamiltonians/Hopping.h"
#include "hamiltonians/ChemicalPotential.h"
#include "hamiltonians/InteractionTerm.h"
#include "hamiltonians/SYK2.h"
#include "hamiltonians/SYK4.h"
#include <boost/algorithm/string.hpp>
#include "Log.h"
#include "observables/EnergyObservable.h"
#include "cli/HamiltonianCreator.h"
#include "cli/StateSelector.h"
#include "cli/SingleBasisStateSelector.h"
#include "cli/BasisStatesSelector.h"
#include "cli/BlockBasisStateSelector.h"
#include "cli/SingleEigenstateSelector.h"
#include "cli/SimulationSelector.h"
#include "cli/ExactSimulationSelector.h"
#include "cli/LanczosSimulationSelector.h"
#include "cli/CLIParser.h"

using namespace std;
using namespace arma;

// ========================= parsing functions ===================================================================
StateSelector *parseInitialStateParam(string param) {
    vector<string> params = Utils::split(param, ',');
    if (params.size() < 1)
        throw runtime_error("Initial state param not parsable");

    string type = params[0];
    params.erase(params.begin());

    if (type == "es") {
        if (params.size() < 1)
            throw runtime_error("parameters for eigenstate selector: nth");

        uint nth = atoi(params[0].c_str());
        if (params.size() > 1)
            return new SingleEigenstateSelector(nth, atoi(params[1].c_str()));
        else
            return new SingleEigenstateSelector(nth);
    } else if (type == "bs") {
        if (params.size() < 1)
            throw runtime_error("parameters for basis state selector: nth,[Q],[occupiedSite]");

        uint nth = atoi(params[0].c_str());
        int Q = params.size() > 1 ? atoi(params[1].c_str()) : -1;
        int occupiedSite = params.size() > 2 ? atoi(params[2].c_str()) : -1;
        return new SingleBasisStateSelector(nth, Q, occupiedSite);
    } else if (type == "bss") {
        if (params.size() < 1)
            throw runtime_error("parameters for basis state selector: nth(vector)");

        vector<uint> nth = Utils::applyTo<uint>(params, [](string s) { return atoi(s.c_str()); });
        return new BasisStatesSelector(nth);
    } else if (type == "bssr") {
        if (params.size() < 2)
            throw runtime_error("parameters for basis state selector: from,to");

        uint from = atoi(params[0].c_str());
        uint to = atoi(params[1].c_str());
        return new BasisStatesSelector(from, to);
    } else if (type == "bs-block") {
        if (params.size() < 1)
            throw runtime_error("parameters for basis state selector: nth");

        uint nth = atoi(params[0].c_str());
        return new BlockBasisStateSelector(nth);
    } else {
        cout
                << "valid initialisation types: 'es' (eigenstate), 'bs' (basis state), 'bss' (list of basis states), 'bssr' (range of basis states), 'bs-block' (basis state from block)"
                << endl;
        exit(0);
    }
}

SimulationSelector *parseTimeEvolutionParam(string param) {
    vector<string> params = Utils::split(param, ',');
    if (params.size() < 3)
        throw runtime_error("Time evolution param not parsable");

    string type = params[0];
    params.erase(params.begin());

    if (type == "exact") {
        if (params.size() < 2)
            throw runtime_error("parameters for exact time evolution: dt nt");

        double dt = atof(params[0].c_str());
        uint nt = atoi(params[1].c_str());
        return new ExactSimulationSelector(dt, nt);
    } else if (type == "lanczos") {
        if (params.size() < 3)
            throw runtime_error("parameters for Lanczos time evolution: dt nt numLanczosVectors");

        double dt = atof(params[0].c_str());
        uint nt = atoi(params[1].c_str());
        uint numLanczosVectors = atoi(params[2].c_str());
        return new LanczosSimulationSelector(dt, nt, numLanczosVectors);
    } else {
        cout << "valid time evolution types: 'exact', 'lanczos'" << endl;
        exit(0);
    }
}

vector<double> parseRange(string param) {
    vector<double> params = Utils::applyTo<double>(Utils::split(param, '/'),
                                                   [](string s) { return atof(s.c_str()); });
    if (params.size() < 3)
        throw runtime_error("energy range: start/stop/step (start and stop inclusive)");

    return Utils::arange(params[0], params[1], params[2]);
}

// ========================= main functions =====================================================================

// entropy density calculation
void mainEntropyDensity(vector<string> params) {
    if (params.size() < 3) {
        cout << "entropy density as function of temperature" << endl;
        cout << "params: Hamiltonian numAverage filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    uint numAverage = atof(params[1].c_str());
    string filename(params[2]);
    vector<double> temperatures = Utils::convert<vec, double>(linspace(0.001, 2.0, 2000));
    vector<double> beta = Utils::applyTo<double>(temperatures, [](double t) { return 1.0 / t; });

    // load file and add temperatures
    StorageFile file(filename);
    if (file.exists())
        file.load();
    else
        file.addColumn(temperatures, "temperature");

    Log::info() << creator->toString() << endl << "output file: " << file.getPath() << endl;

    // calculate entropy density for temperatures averaged over multiple realisations
    vec avg(temperatures.size(), fill::zeros);
    Hamiltonian *H = creator->createHamiltonian();
    for (uint i = 0; i < numAverage; ++i) {
        Log::info() << "\trealisation" << i << endl;

        if (i > 0)
            H->updateMatrix(true);
        H->diagonalise(false);
        avg += Thermo::entropyDensity(H, beta);
    }
    avg /= numAverage;

    // save file
    file.setColumn("N=" + to_string(creator->N), Utils::convert<vec, double>(avg));
    file.save();

    delete H;
    delete creator;
}

// entropy density calculation
void mainSubspaceEntropyDensity(vector<string> params) {
    if (params.size() < 3) {
        cout << "entanglement entropy density in ground state for different subsystem sizes" << endl;
        cout << "params: Hamiltonian numAverage filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    uint numAverage = atof(params[1].c_str());
    string filename(params[2]);

    // test if file can be accessed
    StorageFile file(filename);
    file.save();

    // create Hamiltonian and find full basis
    Log::info() << "create Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();
    Basis basis = H->getBasis();

    vector<uint> subspaceSizes;
    vector<double> entropies;

    // iterate over subsystem sizes
    for (uint n = 0; n <= basis.getN(); n++) {
        uint subspaceSize = min(n, basis.getN() - n);
        Log::info() << "subspace " << n << " (size " << subspaceSize << ")" << endl;

        // calculate entanglement entropy density averaged over multiple realisations
        double average = 0;
        for (uint i = 0; i < numAverage; ++i) {
            // select random subspace
            set<uint> subspace = Utils::selectNRandomly(basis.getSites(), subspaceSize);
            EntropyCalculator calculator(basis, subspace);
            Log::info() << "\trealisation " << i << ", subspace: " << subspace << endl;

            // update and diagonalise Hamiltonian
            if (i > 0) {
                Log::info() << "\t\tupdate Hamiltonian..." << endl;
                H->updateMatrix(true);
            }
            Log::info() << "\t\tfind ground state..." << endl;
            H->diagonalise(true, true);

            // calculate entropy
            Log::info() << "\t\tcalculate entropy..." << endl;
            cx_mat rho = calculator.partialTrace(H->getEigenvectors().col(0));
            average += EntropyCalculator::entropy(rho, true);
        }

        subspaceSizes.push_back(n);
        entropies.push_back(average / numAverage);
    }

    // save file
    file.addColumn(Utils::applyTo<double>(subspaceSizes, [](uint x) { return (double) x; }), "subspace size");
    file.addColumn(entropies, "entanglement entropy");
    file.save();

    delete creator;
}

// spectrum / eigenvalues of multiple realisations
void mainSpectrum(vector<string> params) {
    if (params.size() < 2) {
        cout << "spectrum / eigenvalues of multiple realisations" << endl;
        cout << "params: Hamiltonian numRealisations filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    uint numRealisations = atoi(params[1].c_str());
    string filename(params[2]);

    // open file
    StorageFile file(filename);
    if (file.exists())
        throw runtime_error("file exists!");
    Log::info() << creator->toString() << "output file: " << file.getPath() << endl;

    Log::info() << "setup Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();

    // calculate eigenvalues
    vector<vector<double>> evals;
    for (uint i = 0; i < numRealisations; i++) {
        if (i > 0) {
            Log::info() << i << ": update Hamiltonian..." << endl;
            H->updateMatrix(true);
        }

        Log::info() << i << ": diagonalise..." << endl;
        H->diagonalise(false);
        evals.push_back(Utils::convert<vec, double>(H->getEigenvalues()));
    }

    // save
    file.setColumn("eigenvalues", Utils::combine(evals));
    file.save();

    delete H;
    delete creator;
}

// sparseness of Hamiltonian
void mainSparseness(vector<string> params) {
    if (params.size() < 5) {
        cout << "sparseness" << endl;
        cout << "params: Hamiltonian N1 N2 dN filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    uint N1 = atoi(params[1].c_str());
    uint N2 = atoi(params[2].c_str());
    uint dN = atoi(params[3].c_str());
    string filename(params[4]);

    // open file
    vector<double> Ns, sparseness;
    StorageFile file(filename);
    if (file.exists())
        throw runtime_error("file exists");

    creator->N = N1;
    while (creator->N <= N2) {
        if (creator->Q > 0)
            creator->Q = creator->N / 2;

        Hamiltonian *H = creator->createHamiltonian();
        Ns.push_back(creator->N);
        sparseness.push_back(H->getSparseness());
        Log::info() << "N=" << creator->N << ", Q=" << creator->Q << ", sparseness=" << H->getSparseness() << endl;
        if (dynamic_cast<HamiltonianBlock *>(H)) {
            Log::debug() << "\tsparse matrix: " << dynamic_cast<HamiltonianBlock *>(H)->isUsingSparseMatrix() << endl;
        }

        delete H;
        creator->N += dN;
    }

    file.setColumn("N", Ns);
    file.setColumn("sparseness", sparseness);
    file.save();
}

// single-particle DOS in spectral representation
void mainDoSGF(vector<string> params) {
    if (params.size() < 6) {
        cout
                << "Single particle spectral function (single site) or density of states (trace over sites) in spectral representation"
                << endl;
        cout << "params: Hamiltonian energyRange numAverage eta site filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    vector<double> energies = parseRange(params[1]);
    uint numAverage = atoi(params[2].c_str());
    double eta = atof(params[3].c_str());
    int site = atoi(params[4].c_str());
    string filename(params[5]);

    // load file or add energy column
    StorageFile file(filename);
    if (file.exists())
        file.load();
    else
        file.addColumn(energies, "energy");

    Log::info() << "setup Hamiltonian..." << endl;
    BlockDiagonalHamiltonian *H = static_cast<BlockDiagonalHamiltonian *>(creator->createHamiltonian());

    Log::debug() << "energy range: " << energies.front() << " ... " << energies.back()
                 << " (step " << (energies[1] - energies[0]) << ", length " << energies.size() << ")" << endl;

    // calculate Green's function averaged over multiple realisations
    vec dos(energies.size(), fill::zeros);
    for (int i = 0; i < numAverage; ++i) {
        Log::info() << i << "/" << numAverage << endl;
        if (i > 0) {
            Log::info() << "\tupdating Hamiltonian..." << endl;
            H->updateMatrix(true);
        }

        // grand canonical version, all blocks
        Log::info() << "\tdiagonalising..." << endl;
        H->diagonalise(true, false);

        for (uint Q = 0; Q <= H->getBasis().getN(); Q++) {
            GreensFunctionCalculator calc(H->getBlock(Q), Q < H->getBasis().getN() ? H->getBlock(Q + 1) : nullptr,
                                          Q > 0 ? H->getBlock(Q - 1) : nullptr, eta);
            Log::info() << "\tDoS for block " << Q << ", " << calc.numGroundStates() << " ground states..." << endl;

            if (site < 0)
                dos += calc.densityOfStates(energies);
            else
                dos += calc.spectralFunction(energies, site);
        }
    }
    dos /= numAverage;

    // save data
    file.addColumn(Utils::convert<vec, double>(dos),
                   "DoS N=" + to_string(creator->N) + " V=" + to_string(creator->V) + " E=" + to_string(creator->Eimp));
    file.save();

    delete H;
    delete creator;
}

// time evolution (all sites) without thermalisation or quenching
void mainTimeEvolve(vector<string> params) {
    if (params.size() < 5) {
        cout << "time evolution from initial state" << endl;
        cout << "params: Hamiltonian timeEvolution numAverage initialisation filename" << endl;
        return;
    }

    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    uint numAverage = atoi(params[2].c_str());
    StateSelector *stateSelector = parseInitialStateParam(params[3]);
    string filename(params[4]);

    // open file and add time column
    StorageFile file(filename);
    file.addColumn(simulationSelector->createTimeVector(), "times");
    file.save();

    Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                << "initialisation: " << stateSelector->toString() << endl
                << "output file: " << file.getPath() << endl;

    // setup
    Log::info() << "setup Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();

    vector<vec> averages(H->getBasis().getN(), vec(simulationSelector->nt, fill::zeros));
    vec energy(simulationSelector->nt, fill::zeros);

    for (uint i = 0; i < numAverage; i++) {
        Log::info() << "realisation " << i << endl;
        if (i > 0) {
            Log::info() << "update Hamiltonian..." << endl;
            H->updateMatrix(true);
        }

        // init simulation in ground state
        Log::info() << "init simulation..." << endl;
        Simulation *sim = simulationSelector->createSimulation(H);
        stateSelector->initialise(sim);
        EnergyObservable energyObs(H);
        OccupationObservable occupationObs(H->getBasis());
        Log::info() << "initial occupations=" << occupationObs.calculateOccupations(sim->getPsi()) << endl;

        // run simulation
        Timer timer;
        timer.start();
        Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [&timer](uint step, uint numSteps) {
                                    Log::info() << step << " / " << numSteps;
                                    Log::debug() << "\t" << timer.toString(numSteps);
                                    Log::info() << endl;

                                    timer.nextRound();
                                },
                                {&occupationObs, &energyObs});

        // sum up data
        const vector<vector<double>> &occupations = occupationObs.getOccupations();
        for (uint j = 0; j < occupations.size(); j++) {
            averages[j] += Utils::convert<double, vec>(occupations[j]);
        }
        energy += Utils::convert<double, vec>(energyObs.getEnergies());

        delete sim;
    }

    // average and save data
    file.addColumn(energy / numAverage, "energy");
    for (uint i = 0; i < averages.size(); i++) {
        file.addColumn(averages[i] / numAverage, "site " + to_string(i + 1));
    }
    file.save();

    delete H;
    delete simulationSelector;
    delete stateSelector;
    delete creator;
}

// time evolution (all sites) after a quench
void mainTimeEvolveQuenched(vector<string> params) {
    if (params.size() < 4) {
        cout << "time evolution from eigenstate" << endl;
        cout << "params: Hamiltonian timeEvolution initialisation filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    StateSelector *stateSelector = parseInitialStateParam(params[2]);
    string filename(params[3]);

    // create file and add time column
    StorageFile fileTime(filename);
    fileTime.addColumn(simulationSelector->createTimeVector(), "times");
    fileTime.save();

    Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                << "initialisation: " << stateSelector->toString() << endl
                << "output file: " << fileTime.getPath() << endl;

    // setup
    Log::info() << "setup..." << endl;
    Hamiltonian *H = creator->createHamiltonian();
    if (H->hasImpurity()) {
        H->updateHoppingPotential(0);
        H->updateImpurityEnergy(-1000);
    }

    Log::info() << "diagonalise..." << endl;
    H->diagonalise();

    ExactSimulation sim(H);
    OccupationObservable observable(H->getBasis());
    stateSelector->initialise(&sim);
    Log::info() << "initial occupations=" << observable.calculateOccupations(sim.getPsi()) << endl;

    if (H->hasImpurity()) {
        // add impurity again, quench band
        Log::info() << "diagonalise 2..." << endl;
        H->updateImpurityEnergy(creator->Eimp);
        H->updateHoppingPotential(creator->V);
        H->diagonalise();
    }

    // run time evolution
    Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
    SimulationUtils::evolve(&sim, simulationSelector->nt, simulationSelector->dt,
                            [](uint step, uint numSteps) {
                                Log::info() << step << " / " << numSteps << endl;
                            }, {&observable});

    // save data
    const vector<vector<double>> &occupations = observable.getOccupations();
    for (uint i = 0; i < occupations.size(); i++) {
        fileTime.addColumn(occupations[i], "site " + to_string(i + 1));
    }
    fileTime.save();

    delete creator;
    delete stateSelector;
}

// time evolution (impurity only)
void mainTimeEvolveImpurityQuenched(vector<string> params) {
    if (params.size() < 7) {
        cout << "time evolution of impurity from band's eigenstate (selected by index), averaged over realisations"
             << endl;
        cout << "params: Hamiltonian timeEvolution timeStepsBeforeQuench numAverage initialisation measureSite filename"
             << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    int ntBeforeQuench = atoi(params[2].c_str());
    uint numAverage = atoi(params[3].c_str());
    StateSelector *stateSelector = parseInitialStateParam(params[4]);
    int measureSite = atoi(params[5].c_str());
    string filename(params[6]);

    if (ntBeforeQuench < 0)
        throw runtime_error("timeStepsBeforeQuench must be >= 0");
    double startTime = -simulationSelector->dt * ntBeforeQuench;
    uint totalTimeSteps = ntBeforeQuench + simulationSelector->nt;

    // open file and add time column
    StorageFile file(filename);
    file.addColumn(Utils::linSpaced(totalTimeSteps, startTime,
                                    simulationSelector->nt * simulationSelector->dt), "times");
    file.save();

    Log::info() << creator->toString() << ", " << simulationSelector->toString()
                << ", steps before quench: " << ntBeforeQuench << endl
                << "initialisation: " << stateSelector->toString() << endl
                << "output file: " << file.getPath() << endl;
    Hamiltonian *H = creator->createHamiltonian();

    vector<vec> averageAll;
    for (uint i = 0; i < creator->numSites(); i++) {
        averageAll.push_back(vec(totalTimeSteps, fill::zeros));
    }
    vec average(totalTimeSteps, fill::zeros);
    for (uint i = 0; i < numAverage; i++) {
        // setup
        Log::info() << "realisation " << i << endl;
        if (i > 0) {
            Log::info() << "update Hamiltonian..." << endl;
            H->updateMatrix(true);
        }
        H->updateHoppingPotential(0);
        H->updateImpurityEnergy(-1000);

        // init simulation in ground state
        Log::debug() << "create simulation and observables..." << endl;
        Simulation *sim = simulationSelector->createSimulation(H);
        OccupationObservable observable(H->getBasis());
        stateSelector->initialise(sim);
        Log::info() << "initial occupations=" << observable.calculateOccupations(sim->getPsi()) << endl;

        // run time evolution before quench
        if (ntBeforeQuench > 0) {
            Log::info() << "run before quench (" << ntBeforeQuench << " steps)..." << endl;
            SimulationUtils::evolve(sim, ntBeforeQuench, simulationSelector->dt, [](uint step, uint numSteps) {
                Log::info() << step << " / " << numSteps << endl;
            }, {&observable});
        }

        // add impurity again, quench band
        Log::info() << "quench Hamiltonian..." << endl;
        H->updateImpurityEnergy(creator->Eimp);
        H->updateHoppingPotential(creator->V);

        // run simulation
        Log::info() << "run (" << simulationSelector->nt << " steps)..." << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [](uint step, uint numSteps) {
                                    Log::info() << step << " / " << numSteps << endl;
                                }, {&observable});
        if (measureSite < 0) {
            for (uint j = 0; j < creator->numSites(); j++) {
                averageAll[j] += observable.getOccupations(j);
            }
        } else {
            average += observable.getOccupations(measureSite);
        }

        delete sim;
    }

    // save data
    if (measureSite < 0) {
        for (uint i = 0; i < creator->numSites(); i++) {
            averageAll[i] /= numAverage;
            file.addColumn(averageAll[i], "occupation site " + to_string(i));
        }
    } else {
        average /= numAverage;
        file.addColumn(average, "occupation site " + to_string(measureSite));
    }
    file.save();

    delete H;
    delete simulationSelector;
    delete stateSelector;
    delete creator;
}

// time evolution (impurity only) for different impurity energies
void mainTimeEvolveImpurityQuenchedVarE(vector<string> params) {
    if (params.size() < 7) {
        cout << "time evolution of impurity from band's eigenstate (selected by index), averaged over realisations"
             << endl;
        cout << "params: Hamiltonian timeEvolution Eimp1 Eimp2 dE initialisation filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    double Eimp1 = atof(params[2].c_str());
    double Eimp2 = atof(params[3].c_str());
    double dE = atof(params[4].c_str());
    StateSelector *stateSelector = parseInitialStateParam(params[5]);
    string filename(params[6]);

    // open file and add time column
    StorageFile file(filename);
    file.addColumn(Utils::linSpaced(
            simulationSelector->nt, 0.0, simulationSelector->nt * simulationSelector->dt),
                   "times");
    file.save();

    creator->Eimp = Eimp1;
    while (creator->Eimp <= Eimp2) {
        Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                    << "initialisation: " << stateSelector->toString() << endl
                    << "output file: " << file.getPath() << endl;

        // setup
        Log::info() << "setup..." << endl;
        // terms need to be updated explicitly here
        for (HamiltonianTerm *term : creator->terms) {
            term->update();
        }
        Hamiltonian *H = creator->createHamiltonian();
        H->updateHoppingPotential(0);
        H->updateImpurityEnergy(-1000);

        // init simulation in ground state
        Log::info() << "initialise simulation..." << endl;
        Simulation *sim = simulationSelector->createSimulation(H);
        stateSelector->initialise(sim);
        OccupationObservable observable(H->getBasis());
        Log::info() << "initial occupations=" << observable.calculateOccupations(sim->getPsi()) << endl;

        // add impurity again, quench band
        Log::info() << "quench Hamiltonian..." << endl;
        H->updateImpurityEnergy(creator->Eimp);
        H->updateHoppingPotential(creator->V);

        // run simulation
        Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [](uint step, uint numSteps) {
                                    Log::info() << step << " / " << numSteps << endl;
                                }, {&observable});

        // save data
        file.addColumn(observable.getOccupations(H->getImpurityIndex()), "Eimp=" + to_string(creator->Eimp));
        file.save();

        creator->Eimp += dE;
        delete H;
        delete sim;
    }

    delete simulationSelector;
    delete stateSelector;
    delete creator;
}

// time evolution (impurity only) for multiple eigenstates
void mainTimeEvolveImpurityEigenstateMix(vector<string> params) {
    if (params.size() < 4) {
        cout << "time evolution from band's ground state for different Eimp" << endl;
        cout << "params: Hamiltonian timeEvolution numEigenstates filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    int numEigenstates = atoi(params[2].c_str());
    const string filename(params[3]);

    // open files and add time column
    StorageFile file(filename);
    file.addColumn(simulationSelector->createTimeVector(), "times");
    file.save();
    Log::info() << creator->toString() << ", " << simulationSelector->toString()
                << ", numEigenstates=" << numEigenstates << endl
                << "output file: " << file.getPath() << endl;

    // setup
    Log::info() << "setup..." << endl;
    Hamiltonian *H = creator->createHamiltonian();
    H->updateHoppingPotential(0);
    H->updateImpurityEnergy(-1000);
    Log::info() << "diagonalise..." << endl;
    H->diagonalise(true, false);
    const cx_mat eigenvectors = H->getEigenvectors();
    const vec eigenvalues = H->getEigenvalues();

    // print thermal mixing info
    if (numEigenstates < 0)
        numEigenstates = eigenvalues.size();
    for (uint n = 0; n < numEigenstates; n++) {
        Log::debug() << "eigenstate " << n << ": energy=" << eigenvalues[n]
                     << ", diff=" << eigenvalues[n] - eigenvalues[0] << endl;
    }

    // add impurity again, quench band
    Log::info() << "diagonalise 2..." << endl;
    H->updateImpurityEnergy(creator->Eimp);
    H->updateHoppingPotential(creator->V);

    Timer timer;
    timer.start();
    for (uint n = 0; n < numEigenstates; n++) {
        timer.nextRound();

        // init simulation in unquenched eigenstate
        Simulation *sim = simulationSelector->createSimulation(H);
        OccupationObservable observable(H->getBasis());
        sim->setPsi(eigenvectors.col(n));

        // run simulation
        Log::info() << "run eigenstate " << n << "/" << (numEigenstates < 0 ? H->getBasis().dim() : numEigenstates)
                    << " (" << simulationSelector->nt << " steps)..." << endl
                    << "initial occupations=" << observable.calculateOccupations(sim->getPsi()) << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [](uint step, uint numSteps) {
                                    if (step % 100 == 0)
                                        Log::info() << step << " / " << numSteps << endl;
                                }, {&observable});

        // save data
        vec occ = Utils::convert<double, vec>(observable.getOccupations(H->getImpurityIndex()));
        file.addColumn(occ, "evec" + to_string(n) + "=" + to_string(eigenvalues[n]));
        file.save();

        Log::info() << timer.toString(numEigenstates) << endl;

        delete sim;
    }

    // save eigenvalues in another file
    StorageFile file2(filename + "_eigenvalues");
    file2.addColumn(eigenvalues, "eigenvalues");
    file2.save();

    delete H;
    delete creator;
}

// time evolution for multiple eigenstates, quenched by chemical potential
void mainTimeEvolveChemEigenstateMix(vector<string> params) {
    if (params.size() < 6) {
        cout << "time evolution for multiple eigenstates, quenched by chemical potential" << endl;
        cout << "params: Hamiltonian quenchChem beta timeEvolution numEigenstates filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    double quenchChem = atof(params[1].c_str());
    double beta = atof(params[2].c_str());
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[3]);
    int numEigenstates = atoi(params[4].c_str());
    const string filename(params[5]);

    // open files and add time column
    StorageFile file(filename);
    file.addColumn(simulationSelector->createTimeVector(), "times");
    file.save();
    Log::info() << creator->toString() << ", " << simulationSelector->toString()
                << ", numEigenstates=" << numEigenstates << endl
                << "output file: " << file.getPath() << endl;

    // setup
    Log::info() << "setup..." << endl;
    Hamiltonian *H = creator->createHamiltonian();
    Log::info() << "diagonalise..." << endl;
    H->diagonalise(true);
    const cx_mat eigenvectors = H->getEigenvectors();
    const vec eigenvalues = H->getEigenvalues();
    vec weights = H->boltzmannWeights(beta);

    // print thermal mixing info
    if (numEigenstates < 0)
        numEigenstates = eigenvalues.size();
    for (uint n = 0; n < numEigenstates; n++) {
        Log::debug() << "eigenstate " << n << ": energy=" << eigenvalues[n]
                     << ", diff=" << eigenvalues[n] - eigenvalues[0]
                     << ", weight=" << weights[n] << endl;
    }

    // add impurity again, quench band
    Log::info() << "diagonalise 2..." << endl;
    for (HamiltonianTerm *term : H->getTerms()) {
        ChemicalPotential *chem = dynamic_cast<ChemicalPotential *>(term);
        if (chem != nullptr) {
            chem->setMu(quenchChem);
        }
    }
    H->updateMatrix(true);

    Timer timer;
    timer.start();

    vector<vec> occupation(H->getBasis().getN(), vec(simulationSelector->nt, fill::zeros));
    vec energy(simulationSelector->nt, fill::zeros);

    for (uint n = 0; n < numEigenstates; n++) {
        timer.nextRound();

        // init simulation in unquenched eigenstate
        Simulation *sim = simulationSelector->createSimulation(H);
        OccupationObservable occupationObs(H->getBasis());
        EnergyObservable energyObs(H);
        sim->setPsi(eigenvectors.col(n));

        // run simulation
        Log::info() << "run eigenstate " << n << "/" << (numEigenstates < 0 ? H->getBasis().dim() : numEigenstates)
                    << " (" << simulationSelector->nt << " steps)..." << endl
                    << "initial occupations=" << occupationObs.calculateOccupations(sim->getPsi()) << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [](uint step, uint numSteps) {
                                    if (step % 100 == 0)
                                        Log::info() << step << " / " << numSteps << endl;
                                },
                                {&occupationObs, &energyObs});

        // sum up data
        const vector<vector<double>> &occ = occupationObs.getOccupations();
        for (uint j = 0; j < occ.size(); j++) {
            occupation[j] += Utils::convert<double, vec>(occ[j]) * weights[n];
        }
        energy += Utils::convert<double, vec>(energyObs.getEnergies()) * weights[n];

        Log::info() << timer.toString(numEigenstates) << endl;
        delete sim;
    }

    // save data
    file.addColumn(energy, "energy");
    for (uint i = 0; i < occupation.size(); i++) {
        file.addColumn(occupation[i], "site " + to_string(i + 1));
    }
    file.save();

    delete H;
    delete creator;
}

// subspace entropy measurement during time evolution without thermalisation or quenching
void mainTimeEvolveEntropy(vector<string> params) {
    if (params.size() < 6) {
        cout << "time evolution from initial state" << endl;
        cout << "params: Hamiltonian timeEvolution sites initialisation filename saveOccupations" << endl;
        return;
    }

    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    vector<uint> sites = Utils::applyTo<uint>(Utils::split(params[2], ','), [](string s) {
        return atoi(s.c_str());
    });
    StateSelector *stateSelector = parseInitialStateParam(params[3]);
    string filename(params[4]);
    bool saveOccupations = atoi(params[5].c_str()) == 1;

    // open file and add time column
    StorageFile file(filename);
    if (file.exists())
        throw runtime_error("output file exists");

    Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                << "initialisation: " << stateSelector->toString() << endl
                << "subspace sites: " << sites << endl
                << "output file: " << file.getPath() << endl;

    // setup
    Log::info() << "setup Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();

    // init entropy measurement
    Log::info() << "setup observables..." << endl;
    EntropyObservable entropyObservable(H->getBasis(), set<uint>(sites.begin(), sites.end()));
    OccupationObservable occupationObservable(H->getBasis());

    // init simulation in ground state
    Log::info() << "setup simulation..." << endl;
    Simulation *sim = simulationSelector->createSimulation(H);
    stateSelector->initialise(sim);
    Log::info() << "initial occupations=" << occupationObservable.calculateOccupations(sim->getPsi()) << endl;

    // run simulation
    Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
    SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt, [](uint step, uint numSteps) {
        Log::info() << step << " / " << numSteps << endl;
    }, {&entropyObservable, &occupationObservable});

    // save data
    file.addColumn(sim->getTimes(), "times");
    file.addColumn(entropyObservable.getEntropy(), "entropy");
    if (saveOccupations) {
        for (uint i = 0; i < H->getBasis().getN(); i++) {
            file.addColumn(occupationObservable.getOccupations(i), "occupation site " + to_string(i));
        }
    }
    file.save();

    delete H;
    delete sim;
    delete simulationSelector;
    delete stateSelector;
    delete creator;
}

// subspace entropy measurement during time evolution after quenching
void mainTimeEvolveImpurityQuenchedEntropy(vector<string> params) {
    if (params.size() < 6) {
        cout << "time evolution of impurity from band's eigenstate (selected by index), measurement of subspace entropy"
             << endl;
        cout << "params: Hamiltonian timeEvolution sitesA initialisation filename saveOccupations" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    vector<uint> sites = Utils::applyTo<uint>(Utils::split(params[2], ','), [](string s) {
        return atoi(s.c_str());
    });
    StateSelector *stateSelector = parseInitialStateParam(params[3]);
    string filename(params[4]);
    bool saveOccupations = atoi(params[5].c_str()) == 1;

    // open file and add time column
    StorageFile file(filename);
    file.addColumn(simulationSelector->createTimeVector(), "times");
    file.save();

    Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                << "initialisation: " << stateSelector->toString() << endl
                << "output file: " << file.getPath() << endl;

    // setup
    Log::info() << "setup Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();
    H->updateHoppingPotential(0);
    H->updateImpurityEnergy(-1000);

    // init simulation in ground state
    Log::info() << "initialise..." << endl;
    Simulation *sim = simulationSelector->createSimulation(H);
    stateSelector->initialise(sim);

    // add impurity again, quench band
    Log::info() << "quench Hamiltonian..." << endl;
    H->updateImpurityEnergy(creator->Eimp);
    H->updateHoppingPotential(creator->V);

    // init entropy measurement
    Log::info() << "setup observables..." << endl;
    EntropyObservable entropyObservable(H->getBasis(), set<uint>(sites.begin(), sites.end()));
    OccupationObservable occupationObservable(H->getBasis());
    Log::info() << "initial occupations=" << occupationObservable.calculateOccupations(sim->getPsi()) << endl;

    // run simulation
    Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
    SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                            [](uint step, uint numSteps) {
                                Log::info() << step << " / " << numSteps << endl;
                            }, {&entropyObservable, &occupationObservable});

    // save data
    file.addColumn(entropyObservable.getEntropy(), "entropy");
    if (saveOccupations)
        file.addColumn(occupationObservable.getOccupations(H->getImpurityIndex()), "impurity occupation");
    file.save();

    delete H;
    delete sim;
    delete simulationSelector;
    delete stateSelector;
    delete creator;
}

// time evolution with energy measurement during quench
void mainTimeEvolveQuenchEnergy(vector<string> params) {
    if (params.size() < 6) {
        cout << "time evolution of energy during quench averaged over realisations"
             << endl;
        cout << "params: Hamiltonian timeEvolution timeStepsBeforeQuench numAverage initialisation filename" << endl;
        return;
    }

    // parameters
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    SimulationSelector *simulationSelector = parseTimeEvolutionParam(params[1]);
    uint ntBeforeQuench = atoi(params[2].c_str());
    uint numAverage = atoi(params[3].c_str());
    StateSelector *stateSelector = parseInitialStateParam(params[4]);
    string filename(params[5]);

    if (ntBeforeQuench < 0)
        throw runtime_error("timeStepsBeforeQuench must be >= 0");
    double startTime = -simulationSelector->dt * ntBeforeQuench;
    uint totalTimeSteps = ntBeforeQuench + simulationSelector->nt;

    if (!creator->useImpurity)
        throw runtime_error("Cannot measure quench energy without impurity");

    // open file and add time column
    StorageFile file(filename);
    file.addColumn(Utils::linSpaced(totalTimeSteps, startTime,
                                    simulationSelector->nt * simulationSelector->dt), "times");
    file.save();

    // setup
    Log::info() << "setup Hamiltonian..." << endl;
    Hamiltonian *H = creator->createHamiltonian();

    vec averageEnergy(totalTimeSteps, fill::zeros);
    vec averageOccupation(totalTimeSteps, fill::zeros);
    for (uint i = 0; i < numAverage; i++) {
        Log::info() << creator->toString() << ", " << simulationSelector->toString() << endl
                    << "initialisation: " << stateSelector->toString() << endl
                    << "output file: " << file.getPath() << endl
                    << "realisation " << i << endl;

        if (i > 0) {
            Log::info() << "update Hamiltonian..." << endl;
            H->updateMatrix(true);
        }
        H->updateHoppingPotential(0);
        H->updateImpurityEnergy(-1000);

        // init simulation in ground state
        Simulation *sim = simulationSelector->createSimulation(H);
        EnergyObservable energyObservable(H);
        stateSelector->initialise(sim);
        OccupationObservable occupationObservable(H->getBasis());

        // run before quenching
        if (ntBeforeQuench > 0) {
            Log::info() << "run before quench (" << ntBeforeQuench << ")..." << endl;
            SimulationUtils::evolve(sim, ntBeforeQuench, simulationSelector->dt,
                                    [](uint step, uint numSteps) {
                                        Log::info() << step << " / " << numSteps << endl;
                                    },
                                    {&energyObservable, &occupationObservable});
        }

        // add impurity again, quench band
        Log::info() << "quench Hamiltonian..." << endl;
        H->updateImpurityEnergy(creator->Eimp);
        H->updateHoppingPotential(creator->V);

        // run simulation
        Log::info() << "run (" << simulationSelector->nt << ")..." << endl;
        SimulationUtils::evolve(sim, simulationSelector->nt, simulationSelector->dt,
                                [](uint step, uint numSteps) {
                                    Log::info() << step << " / " << numSteps << endl;
                                },
                                {&energyObservable, &occupationObservable});
        averageEnergy += Utils::convert<double, vec>(energyObservable.getEnergies());
        averageOccupation += Utils::convert<double, vec>(occupationObservable.getOccupations(H->getImpurityIndex()));

        delete sim;
    }

    // save data
    file.addColumn(averageEnergy / numAverage, "energy Eimp=" + to_string(creator->Eimp));
    file.addColumn(averageOccupation / numAverage, "impurity occupation Eimp=" + to_string(creator->Eimp));
    file.save();

    delete simulationSelector;
    delete stateSelector;
    delete creator;
    delete H;
}

void mainEnergyTemperature(vector<string> params) {
    if (params.size() < 3) {
        cout << "ground state and initial energy" << endl;
        cout << "params: Hamiltonian initialisation numAverage" << endl;
        return;
    }

    // parse params
    HamiltonianCreator *creator = CLIParser::parseHamiltonian(params[0]);
    StateSelector *stateSelector = parseInitialStateParam(params[1]);
    uint numAverage = atoi(params[2].c_str());
    Hamiltonian *H = creator->createHamiltonian();

    // measure ground state energy
    double gsEnergy = 0;
    double initialEnergy = 0;
    for (uint i = 0; i < numAverage; i++) {
        Log::debug() << "realisation " << i << endl;
        if (i > 0) {
            Log::debug() << "updating Hamiltonian" << endl;
            H->updateMatrix(true);
        }
        H->diagonalise(true, false);
        EnergyObservable E(H);
        gsEnergy += E.measure(H->getEigenvectors().col(0));

        // measure initial energy
        ExactSimulation sim(H);
        stateSelector->initialise(&sim);
        initialEnergy += E.measure(sim.getPsi());
    }
    gsEnergy /= numAverage;
    initialEnergy /= numAverage;

    Log::info() << "ground state energy: " << gsEnergy << endl
                << "initial energy: " << initialEnergy << endl
                << "difference/temperature: " << (initialEnergy - gsEnergy) << endl
                << "N*T: " << creator->N * (initialEnergy - gsEnergy) << endl;
}

int main(int argc, char *argv[]) {
    map<string, function<void(vector<string> &)>> functions = {
            {"entropydensity",            mainEntropyDensity},
            {"ent-entropy",               mainSubspaceEntropyDensity},
            {"spectrum",                  mainSpectrum},
            {"gf-dos",                    mainDoSGF},
            {"time",                      mainTimeEvolve},
            {"time-quench",               mainTimeEvolveQuenched},
            {"time-impurity-quench",      mainTimeEvolveImpurityQuenched},
            {"time-impurity-quench-ess",  mainTimeEvolveImpurityEigenstateMix},
            {"time-impurity-quench-varE", mainTimeEvolveImpurityQuenchedVarE},
            {"time-chem-quench-ess",      mainTimeEvolveChemEigenstateMix},
            {"sparseness",                mainSparseness},
            {"time-entropy",              mainTimeEvolveEntropy},
            {"time-quench-entropy",       mainTimeEvolveImpurityQuenchedEntropy},
            {"time-quench-energy",        mainTimeEvolveQuenchEnergy},
            {"energy-temperature",        mainEnergyTemperature}
    };

    // convert argv to strings
    vector<string> params;
    for (uint i = 1; i < argc; i++) {
        params.push_back(string(argv[i]));
    }

    // handle optional logging parameter
    if (!params.empty() && boost::algorithm::istarts_with(params[0], "-l")) {
        int level = atoi(params[0].substr(2).c_str());
        if (level < Log::Level::NONE || level > Log::Level::DEBUG)
            throw runtime_error("Valid log levels: " + to_string(Log::Level::NONE) + " ... "
                                + to_string(Log::Level::DEBUG));
        Log::getInstance().setLevel((Log::Level) level);
        cout << "set log level to " << Log::getInstance().getLevel() << endl;

        params.erase(params.begin());
    }

    if (params.empty() || !Utils::contains(functions, params[0])) {
        for (pair<string, function<void(vector<string> &)>> pair : functions) {
            cout << pair.first << endl;
        }

        cout << endl;
        cout << "main parameters for Hamiltonian: N,Q,useImpurity,Eimp,V,[sparse]/[terms]" << endl;
        cout << "valid Hamiltonian terms:" << endl;
        cout << "  'onsite' (on-site energy): bandwidth" << endl;
        cout << "  'chem' (chemical potential): mu" << endl;
        cout << "  'hopping' (constant hopping): energy" << endl;
        cout << "  'syk2' (SYK q=2): J" << endl;
        cout << "  'syk4' (SYK q=4): J" << endl;
        cout << "  'int' (constant interaction): strength" << endl;
        cout << "Time evolution:" << endl;
        cout << "  'exact': dt,nt" << endl;
        cout << "  'lanczos': dt,nt,numLanczosVectors" << endl << endl;
        cout << "Initialisation:" << endl;
        cout << "  eigenstate 'es': nth" << endl;
        cout << "  basis state 'bs': nth" << endl;
        cout << "  basis state with occupied impurity 'bsocc': nth,occupiedSite" << endl;
        cout << "  basis states 'bss': nth1,nth2,..." << endl;
        cout << "  basis state per block 'bs-block': nth" << endl;
        cout << endl << "logging: -ln where n is an integer: 0=none,1=errors,2=infos" << endl;
        return 1;
    }

    // find function and run
    auto func = functions.at(params[0]);
    params.erase(params.begin());
    func(params);

    return 0;
}
