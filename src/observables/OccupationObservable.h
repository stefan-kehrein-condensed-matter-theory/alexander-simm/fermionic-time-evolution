#ifndef IMPURITY_MODEL_OCCUPATIONOBSERVABLE_H
#define IMPURITY_MODEL_OCCUPATIONOBSERVABLE_H

#include <set>
#include <vector>
#include "../Observable.h"
#include "../Basis.h"

class OccupationObservable : public Observable {
public:
    OccupationObservable(const Basis &basis);

    void measure(const arma::cx_vec &psi, uint step, uint numSteps) override;

    double calculateOccupation(const arma::cx_vec &psi, uint site) const;

    std::vector<double> calculateOccupations(const arma::cx_vec &psi) const;

    const std::vector<std::vector<double>> &getOccupations() const;

    const std::vector<double> &getOccupations(uint site) const;

private:
    Basis basis;
    std::vector<std::vector<double>> occupations;
};

#endif //IMPURITY_MODEL_OCCUPATIONOBSERVABLE_H
