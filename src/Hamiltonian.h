#ifndef IMPURITY_MODEL_HAMILTONIAN_H
#define IMPURITY_MODEL_HAMILTONIAN_H

#include <armadillo>
#include <set>
#include "State.h"
#include "Basis.h"
#include "hamiltonians/HamiltonianTerm.h"

/**
 * A Hamiltonian contains multiple terms (instances of HamiltonianTerm) and an optional impurity.
 */
class Hamiltonian {
public:
    /**
     * Creates a Hamiltonian for N sites without impurity.
     *
     * @param N number of band sites without impurity
     */
    Hamiltonian(uint N, std::vector<HamiltonianTerm *> terms);

    /**
     * Creates a Hamiltonian for N+1 sites including an impurity.
     *
     * @param N number of band sites without impurity
     * @param impurityEnergy on-site energy of the impurity
     * @param V hopping energy from and to the impurity
     */
    Hamiltonian(uint N, std::vector<HamiltonianTerm *> terms, double impurityEnergy, double V);

    Hamiltonian(const Hamiltonian &h);

    virtual ~Hamiltonian() = default;

    Hamiltonian &operator=(const Hamiltonian &h);

    /** Returns a list of all terms in this Hamiltonian. */
    std::vector<HamiltonianTerm *> getTerms() const;

    /**
     * Returns the basis of the Hilbert space on which this Hamiltonian is defined. The index of each basis state
     * corresponds to the index in the state vector.
     */
    virtual const Basis &getBasis() const = 0;

    /** Returns whether this system includes an impurity. */
    bool hasImpurity() const;

    /** Returns the on-site energy of the impurity or NaN if the system does not includes an impurity. */
    double getImpurityEnergy() const;

    /**
     * Sets the impurity's on-site energy to a new value and updates the Hamiltonian matrix. This clears the
     * diagonalisation status and all eigenvalues and eigenvectors.
     *
     * @param E new value for the impurity energy
     */
    virtual void updateImpurityEnergy(double E) = 0;

    /**
     * Returns the hopping energy between the band and the impurity or NaN if the system does not includes an
     * impurity.
     */
    double getHoppingPotential() const;

    /**
     * Sets the impurity's hopping energy to a new value and updates the Hamiltonian matrix. This clears the
     * diagonalisation status and all eigenvalues and eigenvectors.
     *
     * @param E new value for the hopping energy
     */
    virtual void updateHoppingPotential(double V) = 0;

    /** Returns the index of the impurity in the state vector or -1 if the system does not includes an impurity. */
    int getImpurityIndex() const;

    /** Returns the indices of all band sites in the state vector. */
    std::set<uint> getBandSites() const;

    /**
     * Returns whether the Hamiltonian uses sparse matrices. This determines whether asMatrix() or
     * asSparseMatrix() return a valid matrix representation.
     */
    virtual bool isUsingSparseMatrix() const = 0;

    /** Return the full matrix form of the Hamiltonian. */
    virtual arma::cx_mat asMatrix() const = 0;

    /** Return the sparse matrix form of the Hamiltonian, if it is using one. */
    virtual arma::sp_cx_mat asSparseMatrix() const = 0;

    /** Returns whether this Hamiltonian was already diagonalised. */
    virtual bool isDiagonalised() const = 0;

    /**
     * Diagonalises thie Hamiltonian and stores the resulting eigenvalues and eigenvectors.
     *
     * @param includeEigenvectors whether the eigenvectors should be calculated, too. If false, only eigenvalues
     * will be calculated and stored.
     * @param onlyGroundState if only the ground state or all eigenstates should be found
     */
    virtual void diagonalise(bool includeEigenvectors = true, bool onlyGroundState = false) = 0;

    /** Returns the eigenvalues of this Hamiltonian, if it was diagonalised. */
    virtual const arma::vec &getEigenvalues() const = 0;

    /** Returns the eigenvectors of this Hamiltonian as the columns of a matrix, if it was diagonalised. */
    virtual const arma::cx_mat &getEigenvectors() const = 0;

    /** Calculates the canonical partition function for a given beta. The Hamiltonian needs to be diagonalised for this. */
    double partitionFunction(double beta) const;

    /**
     * Returns the Boltzmann weights for a given inverse temperature beta in the same order as the eigenvalues.
     * The weights are already normalised by the partition function.
     */
    arma::vec boltzmannWeights(double beta) const;

    /** Calculates the average energy for a given beta. The Hamiltonian needs to be diagonalised for this. */
    double averageEnergy(double beta) const;

    /** Returns the percentage of non-null entries in this HamiltonianBlock. */
    virtual double getSparseness() const = 0;

    /**
     * Reinitialises and fills the matrix and, optionally, updates all terms are before. If updated, all terms
     * that contain random numbers will be filled with new numbers. This needs to be done after modifying any
     * of the terms' parameters and clears the diagonalisation status and all eigenvalues and eigenvectors.
     */
    virtual void updateMatrix(bool updateTerms) = 0;

protected:
    /** Index of the impurity. Value of -1 means no impurity. */
    int impurityIndex;
    /** Indices of the band (non-impurity) sites */
    std::set<uint> bandSites;
    /** On-site energy of the impurity */
    double impurityEnergy;
    /** Hopping potential for impurity */
    double impurityHopping;
    /** List of all terms. */
    std::vector<HamiltonianTerm *> terms;
};

#endif //IMPURITY_MODEL_HAMILTONIAN_H
