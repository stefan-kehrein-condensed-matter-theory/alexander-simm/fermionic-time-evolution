#ifndef IMPURITY_MODEL_STATESELECTOR_H
#define IMPURITY_MODEL_STATESELECTOR_H

#include <string>
#include "../simulations/Simulation.h"

/**
 * Abstract base class for any class that can initialise a simulation with an initial state.
 */
class StateSelector {
public:
    StateSelector() {}

    virtual ~StateSelector() = default;

    /**
     * Initialises the given simulation with an initial state.
     */
    virtual void initialise(Simulation *sim) = 0;

    /**
     * Returns a printable description of this class.
     */
    virtual std::string toString() = 0;
};

#endif //IMPURITY_MODEL_STATESELECTOR_H
