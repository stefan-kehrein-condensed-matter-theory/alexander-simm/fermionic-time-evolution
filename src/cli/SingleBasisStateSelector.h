#ifndef IMPURITY_MODEL_SINGLEBASISSTATESELECTOR_H
#define IMPURITY_MODEL_SINGLEBASISSTATESELECTOR_H

#include "StateSelector.h"

/**
 * Utility class that initialises a simulation in the n-th state of the basis of the Hilbert/Fock space.
 */
class SingleBasisStateSelector : public StateSelector {
public:
    uint nth;
    int Q;
    int occupiedSite;

    /**
     * Creates a new instance. The fermion number specifies the Hilbert subspace from which the n-th state will be
     * selected. If Q<0, the n-th state of the full Fock space will be used. If occupiedSite is a valid site index
     * (>=0) the index nth refers to the n-th state of all basis states of the Hilbert/Fock space in which a
     * specified site is occupied will be selected.
     *
     * @param nth index of the basis state
     * @param Q number of fermions
     * @param occupiedSite site that needs to be occupied, or -1 to ignore occupation
     */
    SingleBasisStateSelector(uint nth, int Q = -1, int occupiedSite = -1) : nth(nth), Q(Q),
                                                                            occupiedSite(occupiedSite) {}

    ~SingleBasisStateSelector() = default;

    void initialise(Simulation *sim) override;

    std::string toString() override;
};


#endif //IMPURITY_MODEL_SINGLEBASISSTATESELECTOR_H
