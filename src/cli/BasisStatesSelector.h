#ifndef IMPURITY_MODEL_BASISSTATESSELECTOR_H
#define IMPURITY_MODEL_BASISSTATESSELECTOR_H

#include "StateSelector.h"

/**
 * Initialises a simulation in a superposition of basis states.
 */
class BasisStatesSelector : public StateSelector {
public:
    std::vector<uint> nth;

    /**
     * Creates an instance with a list of indices of the basis states to be selected. If occupiedSite is valid
     * (>= 0) only states in which the specified site is occupied will be selected.
     */
    BasisStatesSelector(std::vector<uint> nth);

    /**
     * Creates an instance that will select all basis states in a range of indices [from,to). The to index
     * is exclusive.
     */
    BasisStatesSelector(uint from, uint to);

    ~BasisStatesSelector() = default;

    void initialise(Simulation *sim) override;

    std::string toString() override;
};

#endif //IMPURITY_MODEL_BASISSTATESSELECTOR_H
