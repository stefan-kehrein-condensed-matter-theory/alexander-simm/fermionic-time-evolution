#include "BasisStatesSelector.h"
#include "../utils.h"
#include "../Log.h"

using namespace std;

BasisStatesSelector::BasisStatesSelector(std::vector<uint> nth) : nth(nth) {
}

BasisStatesSelector::BasisStatesSelector(uint from, uint to) {
    nth = Utils::arange(from, to);
}

void BasisStatesSelector::initialise(Simulation *sim) {
    Hamiltonian *H = sim->getHamiltonian();
    vector<State> states = Utils::applyTo<State>(nth, [&H](uint n) {
        return H->getBasis().getState(n);
    });
    Log::debug() << "initialising in basis states: " << states << endl;
    sim->setPsi(states);
}

string BasisStatesSelector::toString() {
    stringstream stream;
    stream << "basis states (" << nth << ")";
    return stream.str();
}