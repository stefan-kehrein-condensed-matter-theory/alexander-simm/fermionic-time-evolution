#ifndef IMPURITY_MODEL_BLOCKBASISSTATESELECTOR_H
#define IMPURITY_MODEL_BLOCKBASISSTATESELECTOR_H

#include "StateSelector.h"

/**
 * Initialises a simulation in a superposition of basis states, taking the n-th basis state from each block.
 */
class BlockBasisStateSelector : public StateSelector {
public:
    uint nth;

    BlockBasisStateSelector(uint nth);

    ~BlockBasisStateSelector() = default;

    void initialise(Simulation *sim) override;

    std::string toString() override;
};

#endif //IMPURITY_MODEL_BLOCKBASISSTATESELECTOR_H
