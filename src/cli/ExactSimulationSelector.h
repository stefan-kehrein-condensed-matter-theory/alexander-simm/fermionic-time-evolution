#ifndef IMPURITY_MODEL_EXACTSIMULATIONSELECTOR_H
#define IMPURITY_MODEL_EXACTSIMULATIONSELECTOR_H

#include "SimulationSelector.h"
#include "../simulations/ExactSimulation.h"

/**
 * Utility class that creates simulations for exact time evolution.
 */
class ExactSimulationSelector : public SimulationSelector {
public:
    ExactSimulationSelector(double dt, uint nt);

    Simulation *createSimulation(Hamiltonian *H) override;

    std::string toString() override;
};

#endif //IMPURITY_MODEL_EXACTSIMULATIONSELECTOR_H
