#ifndef IMPURITY_MODEL_HAMILTONIANCREATOR_H
#define IMPURITY_MODEL_HAMILTONIANCREATOR_H

#include <string>
#include <vector>
#include "../Hamiltonian.h"

/**
 * Utility class that holds all parameters of a system and can be used to repeatedly create Hamiltonians (e.g. for
 * random realisations). After creation the list of terms must be filled before Hamiltonians can be created.
 */
class HamiltonianCreator {
public:
    uint N;
    int Q;
    bool useImpurity;
    double Eimp, V;
    bool useSparse;
    /**
     * A list of the terms for the Hamiltonian. This must be filled before creating the Hamiltonian.
     */
    std::vector<HamiltonianTerm *> terms;

    /**
     * Initialised a new creator class.
     *
     * @param N number of sites (without an impurity), or -1 for a grand-canonical Hamiltonian
     * @param Q number of fermions
     * @param useImpurity whether an impurity should be included
     * @param Eimp energy of the impurity, ignored if useImpurity=false
     * @param V hopping energy to/from the impurity, ignored if useImpurity=false
     * @param useSparse whether the underlying matrix should be sparse
     */
    HamiltonianCreator(uint N, uint Q, bool useImpurity, double Eimp, double V, bool useSparse = false);

    ~HamiltonianCreator();

    /**
     * Creates a Hamiltonian instance with the parameters that were specified before. If the number of sites is not
     * specified (N=-1) it will be a block-diagonal Hamiltonian, otherwise it is a single block.
     */
    Hamiltonian *createHamiltonian();

    /**
     * Returns a printable string of all parameters.
     */
    std::string toString();

    /**
     * Returns the number of sites of the system, either N or N+1 for a system with an impurity.
     */
    uint numSites();
};

#endif //IMPURITY_MODEL_HAMILTONIANCREATOR_H
