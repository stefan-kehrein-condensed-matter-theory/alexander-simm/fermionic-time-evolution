#include "LanczosSimulationSelector.h"

using namespace std;

LanczosSimulationSelector::LanczosSimulationSelector(double dt, uint nt, uint numLanczosVectors)
        : SimulationSelector(dt, nt), numLanczosVectors(numLanczosVectors) {}

Simulation *LanczosSimulationSelector::createSimulation(Hamiltonian *H) {
    return new LanczosSimulation(H, numLanczosVectors);
}

string LanczosSimulationSelector::toString() {
    stringstream stream;
    stream << "Lanczos evolution (dt=" << dt << ", nt=" << nt << ", numLanczos=" << numLanczosVectors << ")";
    return stream.str();
}
