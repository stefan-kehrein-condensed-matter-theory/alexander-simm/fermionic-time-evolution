#include "ExactSimulationSelector.h"

using namespace std;

ExactSimulationSelector::ExactSimulationSelector(double dt, uint nt) : SimulationSelector(dt, nt) {
}

Simulation *ExactSimulationSelector::createSimulation(Hamiltonian *H) {
    return new ExactSimulation(H, true);
}

string ExactSimulationSelector::toString() {
    stringstream stream;
    stream << "exact evolution (dt=" << dt << ", nt=" << nt << ")";
    return stream.str();
}
