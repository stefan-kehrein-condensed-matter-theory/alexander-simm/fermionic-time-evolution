#include "Basis.h"
#include <stdexcept>
#include "utils.h"

using namespace std;

Basis::Basis(uint N) : Basis(N, -1) {
}

Basis::Basis(uint N, int Q) : N(N), Q(Q) {
    if (Q < 0) {
        ulong max = 1u << N;
        for (uint q = 0; q <= N; q++) {
            for (ulong a = 0; a < max; a++) {
                State state(N, a);
                if (state.numParticles() == q) {
                    states.push_back(state);
                }
            }
        }
    } else if (Q == 1) {
        for (uint a = 0; a < N; a++) {
            State state(N, 1ul << a);
            states.push_back(state);
        }
    } else {
        ulong max = 1u << N;
        for (ulong a = 0; a < max; a++) {
            State state(N, a);
            if (state.numParticles() == Q) {
                states.push_back(state);
            }
        }
    }

    // fill state index map
    stateIndices.clear();
    for (uint i = 0; i < states.size(); ++i) {
        stateIndices.insert({states[i].getStateNumber(), i});
    }
}

Basis::Basis(const Basis &b) {
    this->operator=(b);
}

Basis &Basis::operator=(const Basis &b) {
    if (this != &b) {
        N = b.N;
        Q = b.Q;
        states = b.states;
        stateIndices = b.stateIndices;
    }

    return *this;
}

uint Basis::getN() const {
    return N;
}

uint Basis::getQ() const {
    return Q;
}

ulong Basis::dim() const {
    return states.size();
}

set<uint> Basis::getSites() const {
    return Utils::toSet(Utils::arange(0, N));
}

const vector<State> &Basis::getStates() const {
    return states;
}

const State &Basis::getState(uint index) const {
    if (index < 0 || index >= states.size())
        throw runtime_error("Invalid state index " + to_string(index) + " in basis with "
                            + to_string(states.size()) + " states");
    return states[index];
}

int Basis::getStateIndex(const State &state) const {
    if (state.isZero())
        return -1;

    auto it = stateIndices.find(state.getStateNumber());
    return (it != stateIndices.end()) ? it->second : -1;
}