#ifndef IMPURITY_MODEL_UTILS_H
#define IMPURITY_MODEL_UTILS_H

#include <ostream>
#include <vector>
#include <armadillo>
#include <map>
#include <complex>
#include <iostream>
#include <set>
#include <algorithm>
#include <random>

//TODO: rename this file to upper case
class Utils {
public:
    /**
     * Prints a vector to an output stream.
     */
    template<class T>
    static void printVector(std::ostream &out, const std::vector<T> &v, const std::string &delimiter = " ",
                            bool includeBrackets = true) {
        if (includeBrackets)
            out << "(";
        for (typename std::vector<T>::const_iterator iter = v.begin(); iter != v.end(); ++iter) {
            if (iter != v.begin())
                out << delimiter;
            out << *iter;
        }
        if (includeBrackets)
            out << " )";
    }

    /**
     * Converts an STL vector into an armadillo vector.
     */
    template<class S, class D>
    static D convert(const std::vector<S> &v) {
        D col(v.size());
        for (int i = 0; i < v.size(); ++i) {
            col(i) = v[i];
        }
        return col;
    }

    /**
     * Converts an armadillo vector into and STL vector.
     */
    template<class S, class D>
    static std::vector<D> convert(const S &v) {
        std::vector<D> col(v.size());
        for (int i = 0; i < v.size(); ++i) {
            col[i] = v(i);
        }
        return col;
    }

    /**
     * Applies a given lambda function to all elements of the vector and returns a new vector with the results.
     */
    template<class T1, class T2, typename F>
    static std::vector<T1> applyTo(const std::vector<T2> &src, F func) {
        std::vector<T1> dst;
        for (uint i = 0; i < src.size(); ++i) {
            dst.push_back(func(src[i]));
        }
        //transform(src.cbegin(), src.cend(), dst.begin(), func); // needs default constructor
        return dst;
    }

    template<typename F>
    static arma::vec applyTo(const arma::vec &src, F func) {
        arma::vec dst = src;
        dst.transform(func);
        return dst;
    }

    /**
     * Returns whether the map contains the given key.
     */
    template<class K, class V>
    static bool contains(const std::map<K, V> &map, K key) {
        return map.find(key) != map.end();
    }

    template<class T>
    static bool contains(const std::vector<T> &v, T key) {
        return indexOf(v, key) >= 0;
    }

    template<class T>
    static int indexOf(const std::vector<T> &v, T key) {
        auto it = std::find(v.begin(), v.end(), key);
        if (it != v.end())
            return distance(v.begin(), it);
        else
            return -1;
    }

    /**
     * Returns the binomial n over k.
     */
    static int binomial(int n, int k);

    /**
     * Approximates the delta distribution at point x by a Lorentzian with a tiny (infinitesimal) eta.
     * @param x parameter of the delta
     * @param eta width of the Lorentzian
     */
    static double delta(double x, double eta);

    static std::vector<double> linSpaced(uint num, double min, double max);

    static std::vector<double> arange(double min, double max, double step);

    /**
     * Creates a list with all integers from min (inclusive) to max (exclusive).
     */
    static std::vector<uint> arange(uint min, uint max);

    template<class T>
    static std::set<T> difference(std::set<T> set1, std::set<T> set2) {
        std::vector<T> dst(std::max(set1.size(), set2.size()));
        auto it = std::set_difference(set1.begin(), set1.end(), set2.begin(), set2.end(), dst.begin());
        dst.resize(it - dst.begin());

        return std::set<T>(dst.begin(), dst.end());
    }

    /**
     * Scales every element of vec with the corresponding elements in factors.
     */
    static std::vector<double> rescale(const std::vector<double> &vec, const std::vector<double> &factors);

    template<class T>
    static void deleteElements(const std::vector<T *> &v) {
        for (T *t : v) {
            delete t;
        }
    }

    template<class T>
    static std::vector<T> normalise(const std::vector<T> &src) {
        const T norm = std::accumulate(src.begin(), src.end(), static_cast<T>(0));
        return applyTo<T>(src, [norm](T x) { return x / norm; });
    }

    template<class T>
    static T selectRandomly(const std::vector<T> v) {
        if (v.empty())
            throw std::runtime_error("vector is empty");
        std::mt19937 rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());
        std::uniform_int_distribution<int> dist(0, v.size() - 1);
        return v[dist(rng)];
    }

    template<class T>
    static std::vector<T> selectNRandomly(const std::vector<T> v, uint n) {
        if (v.empty())
            throw std::runtime_error("vector is empty");
        std::vector<T> copy(v);
        auto rd = std::random_device{};
        auto rng = std::default_random_engine{rd()};
        shuffle(copy.begin(), copy.end(), rng);
        return std::vector<T>(copy.begin(), copy.begin() + n);
    }

    template<class T>
    static std::set<T> selectNRandomly(const std::set<T> s, uint n) {
        if (s.empty())
            throw std::runtime_error("set is empty");

        // create copy
        std::vector<T> copy;
        std::copy(s.begin(), s.end(), std::back_inserter(copy));

        // shuffle copy
        auto rd = std::random_device{};
        auto rng = std::default_random_engine{rd()};
        shuffle(copy.begin(), copy.end(), rng);

        // select subvector of copy
        return std::set<T>(copy.begin(), copy.begin() + n);
    }

    template<class T>
    static void appendAll(const std::vector<std::vector<T>> &src, std::vector<std::vector<T>> &dst) {
        if (src.size() != dst.size())
            throw std::runtime_error("source and destination have different size");

        for (uint i = 0; i < src.size(); i++) {
            std::copy(src[i].begin(), src[i].end(), std::back_inserter(dst[i]));
        }
    }

    template<class T>
    static std::vector<T> combine(const std::vector<std::vector<T>> &src) {
        std::vector<T> dst;
        for (const std::vector<T> &v : src) {
            std::copy(v.begin(), v.end(), std::back_inserter(dst));
        }
        return dst;
    }

    static std::vector<std::string> split(std::string s, char delimiter);

    template<class T>
    static uint firstIndexGreaterThan(std::vector<T> vec, T value, bool greaterEqual = false) {
        auto iter = std::find_if(vec.begin(), vec.end(), [value, greaterEqual](T t) {
            return greaterEqual ? (t >= value) : (t > value);
        });
        if (iter == vec.end())
            throw std::runtime_error("no element greater than " + std::to_string(value) + " found in vector");

        return std::distance(vec.begin(), iter);
    }

    /**
     * Returns a range of a vector. From and to are inclusive.
     */
    template<typename T>
    static std::vector<T> slice(std::vector<T> const &vec, int from, int to) {
        auto first = vec.cbegin() + from;
        auto last = vec.cbegin() + to + 1;
        return std::vector<T>(first, last);
    }

    /**
     * Converts a vector to a set. Any duplicate elements will only appear once in the set.
     */
    template<typename T>
    static std::set<T> toSet(std::vector<T> v) {
        return std::set<T>(v.begin(), v.end());
    }
};

/**
 * Stream print operator for STL vectors.
 */
template<class T>
inline std::ostream &operator<<(std::ostream &out, const std::vector<T> &v) {
    Utils::printVector(out, v);
    return out;
}

/**
 * Stream print operator for STL sets.
 */
template<class T>
inline std::ostream &operator<<(std::ostream &out, const std::set<T> &s) {
    std::vector<T> copy;
    std::copy(s.begin(), s.end(), std::back_inserter(copy));
    Utils::printVector(out, copy);
    return out;
}

template<class T>
std::vector<T> &operator*=(std::vector<T> &v, const T d) {
    for (uint i = 0; i < v.size(); i++) {
        v[i] *= d;
    }
    return v;
}

template<class T>
std::vector<T> operator*(const std::vector<T> &v, const T d) {
    std::vector<T> dst(v.size());
    for (uint i = 0; i < v.size(); i++) {
        dst[i] = v[i] * d;
    }
    return dst;
}

template<class T>
std::vector<T> &operator/=(std::vector<T> &v, const T d) {
    for (uint i = 0; i < v.size(); i++) {
        v[i] /= d;
    }
    return v;
}

template<class T>
std::vector<T> operator/(const std::vector<T> &v, const T d) {
    std::vector<T> dst(v.size());
    for (uint i = 0; i < v.size(); i++) {
        dst[i] = v[i] / d;
    }
    return dst;
}

template<class T>
std::vector<T> &operator+=(std::vector<T> &v, const std::vector<T> w) {
    for (uint i = 0; i < v.size(); i++) {
        v[i] += w[i];
    }
    return v;
}

template<class T>
std::vector<T> operator+(const std::vector<T> &v1, const std::vector<T> v2) {
    std::vector<T> dst(v1.size());
    for (uint i = 0; i < v1.size(); i++) {
        dst[i] = v1[i] + v2[i];
    }
    return dst;
}

template<class T>
std::vector<T> &operator-=(std::vector<T> &v, const std::vector<T> w) {
    for (uint i = 0; i < v.size(); i++) {
        v[i] -= w[i];
    }
    return v;
}

template<class T>
std::vector<T> operator-(const std::vector<T> &v1, const std::vector<T> v2) {
    std::vector<T> dst(v1.size());
    for (uint i = 0; i < v1.size(); i++) {
        dst[i] = v1[i] - v2[i];
    }
    return dst;
}

#endif //IMPURITY_MODEL_UTILS_H
