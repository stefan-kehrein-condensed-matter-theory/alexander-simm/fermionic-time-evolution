#include "ExactSimulation.h"
#include <stdexcept>

using namespace std;
using namespace arma;

static constexpr std::complex<double> I;

ExactSimulation::ExactSimulation(Hamiltonian *system, bool autoDiagonalise)
        : Simulation(system), autoDiagonalise(autoDiagonalise), psiTmp(system->getBasis().dim()) {
}

ExactSimulation::ExactSimulation(const ExactSimulation &s) : Simulation(s) {
    this->operator=(s);
}

ExactSimulation &ExactSimulation::operator=(const ExactSimulation &s) {
    if (this != &s) {
        Simulation::operator=(s);
        autoDiagonalise = s.autoDiagonalise;
    }

    return *this;
}

void ExactSimulation::evolve(double t) {
    if (!system->isDiagonalised()) {
        if (autoDiagonalise)
            system->diagonalise();
        else
            throw runtime_error("System is not diagonalised");
    }

    psiTmp.fill(complex<double>(0, 0));

    const vec &evals = system->getEigenvalues();
    const cx_mat &evecs = system->getEigenvectors();

    const complex<double> it = -I * t;
    complex<double> amplitude;
    for (uint n = 0; n < evals.size(); ++n) {
        amplitude = cdot(evecs.col(n), psi);
        psiTmp += amplitude * exp(it * evals[n]) * evecs.col(n);
    }

    psi = psiTmp;

    update(t);
}