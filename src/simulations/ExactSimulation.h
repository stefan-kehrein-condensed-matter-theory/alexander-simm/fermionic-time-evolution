#ifndef IMPURITY_MODEL_EXACTSIMULATION_H
#define IMPURITY_MODEL_EXACTSIMULATION_H

#include "Simulation.h"

class ExactSimulation : public Simulation {
public:
    explicit ExactSimulation(Hamiltonian *system, bool autoDiagonalise = false);

    ExactSimulation(const ExactSimulation &s);

    ~ExactSimulation() = default;

    ExactSimulation &operator=(const ExactSimulation &s);

    void evolve(double t) override;

private:
    static constexpr std::complex<double> I = std::complex<double>(0, 1);

    bool autoDiagonalise;
    arma::cx_vec psiTmp;
};

#endif //IMPURITY_MODEL_EXACTSIMULATION_H
