#include "LanczosSimulation.h"
#include <stdexcept>

using namespace std;
using namespace arma;

static constexpr std::complex<double> I;

LanczosSimulation::LanczosSimulation(Hamiltonian *system, uint dimLanczos) : Simulation(system),
                                                                             dimLanczos(dimLanczos) {
}

LanczosSimulation::LanczosSimulation(const LanczosSimulation &s) : Simulation(s) {
    this->operator=(s);
}

LanczosSimulation &LanczosSimulation::operator=(const LanczosSimulation &s) {
    if (this != &s) {
        Simulation::operator=(s);
        dimLanczos = s.dimLanczos;
    }

    return *this;
}

void LanczosSimulation::createLanczosBasis(cx_mat &Q, cx_mat &h) const {
    cx_vec alpha(Q.n_cols);
    cx_vec beta(Q.n_cols);

    beta(0) = 0;
    Q.col(0) = normalise(psi);

    //TODO: use directly instead of copy
    bool useSparse = system->isUsingSparseMatrix();
    cx_mat H;
    sp_cx_mat Hsparse;
    if (useSparse)
        Hsparse = system->asSparseMatrix();
    else
        H = system->asMatrix();

    for (int j = 1; j < Q.n_cols; ++j) {
        cx_vec q;
        if (useSparse)
            q = Hsparse * Q.col(j - 1);
        else
            q = H * Q.col(j - 1);

        alpha(j - 1) = cdot(Q.col(j - 1), q);

        q -= alpha(j - 1) * Q.col(j - 1);
        if (j > 1) {
            q -= beta(j - 2) * Q.col(j - 2);
        }

        // reorthogonalize
        auto delta = cdot(Q.col(j - 1), q);
        q -= Q.col(j - 1) * delta;
        alpha(j - 1) += delta;

        beta(j - 1) = norm(q);
        if (fabs(beta(j - 1).real()) <= 1e-14) {
            throw runtime_error("Zero beta value in Lanczos routine");
        }

        Q.col(j) = q / beta(j - 1);
    }

    // store in the projected Hamiltonian
    h.diag() = alpha.tail(Q.n_cols);
    h.diag(-1) = beta.head(Q.n_cols - 1);
    h.diag(+1) = beta.head(Q.n_cols - 1);

    // last diagonal has to be set separately
    const int i = Q.n_cols - 1;
    if (useSparse)
        h(i, i) = cdot(Q.col(i), Hsparse * Q.col(i));
    else
        h(i, i) = cdot(Q.col(i), H * Q.col(i));
}

void LanczosSimulation::evolve(double t) {
    // basis in Krylov subspace
    cx_mat Q(psi.size(), dimLanczos, fill::zeros);
    // H projected into the Krylov subspace
    cx_mat h(dimLanczos, dimLanczos, fill::zeros);

    createLanczosBasis(Q, h);

    h *= complex<double>(0, -t);
    cx_mat tmp = Q * expmat(h);
    psi = tmp.col(0);

    update(t);
}