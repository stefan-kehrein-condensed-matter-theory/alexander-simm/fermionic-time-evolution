#ifndef IMPURITY_MODEL_ENTROPYCALCULATOR_H
#define IMPURITY_MODEL_ENTROPYCALCULATOR_H

#include <armadillo>
#include <set>
#include "../Basis.h"

class EntropyCalculator {
public:
    EntropyCalculator(const Basis &fullBasis, std::set<uint> sitesA);

    EntropyCalculator(const EntropyCalculator &e);

    EntropyCalculator &operator=(const EntropyCalculator &e);

    ~EntropyCalculator() = default;

    std::set<uint> getSitesA() const;

    std::set<uint> getSitesB() const;

    /**
     * Returns the density matrix of a pure state.
     */
    static arma::cx_mat densityMatrix(const arma::cx_vec &psi);

    /**
     * Returns the reduced density matrix of a pure state with subspace B traced out.
     */
    arma::cx_mat partialTrace(const arma::cx_vec &psi) const;

    /**
     * Calculates the entropy S = -tr(rho * log(rho)) for the given reduced density matrix rho.
     * @param rho any (reduced) density matrix
     * @param diagonalise whether the entropy should be calculated from the eigenvalues of the density matrix
     * or by using Armadillo's matrix logarithm. Diagonalisation seems to be faster most of the time.
     */
    static double entropy(const arma::cx_mat &rho, bool diagonalise = true);

private:
    static constexpr double ENTROPY_EIGENVALUE_CUTOFF = 1e-10;

    std::set<uint> sitesA, sitesB;
    Basis fullBasis, basisA, basisB;
    arma::imat stateMatrix;
};

#endif //IMPURITY_MODEL_ENTROPYCALCULATOR_H
