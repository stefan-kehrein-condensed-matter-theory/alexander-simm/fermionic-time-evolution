#ifndef IMPURITY_MODEL_TIMER_H
#define IMPURITY_MODEL_TIMER_H

#include <string>
#include <chrono>

/**
 * Measures and prints computation time.
 */
class Timer {
public:
    Timer();

    ~Timer() = default;

    void start();

    void nextRound();

    /**
     * Returns a string with computation time info.
     * @param numTotalRounds total number of rounds for the estimation of the overall time
     */
    std::string toString(uint numTotalRounds);

private:
    uint nthRound;
    std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::nanoseconds> startTime, roundStartTime;

    static void formatDuration(std::chrono::high_resolution_clock::duration duration, std::stringstream &out);
};

#endif //IMPURITY_MODEL_TIMER_H
