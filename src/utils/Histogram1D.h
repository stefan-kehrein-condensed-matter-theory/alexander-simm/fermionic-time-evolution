#ifndef IMPURITY_MODEL_HISTOGRAM1D_H
#define IMPURITY_MODEL_HISTOGRAM1D_H

#include <vector>
#include <numeric>
#include <algorithm>
#include <cmath>

template<typename T>
class Histogram1D {
public:
    explicit Histogram1D(uint numBins, T min, T max) : numBins(numBins), min(min), max(max) {
        binSize = (max - min) / numBins;
        data = std::vector<uint>(numBins, 0);
    };

    T getMin() const {
        return min;
    }

    T getMax() const {
        return max;
    }

    uint getNumBins() const {
        return numBins;
    }

    std::vector<uint> getHistogram() const {
        return data;
    }

    std::pair<T, T> getLimits() const {
        return std::pair<T, T>(min, max);
    }

    T getBinSize() const {
        return binSize;
    }

    void update(T value) {
        if (value >= min && value <= max) {
            uint index = std::floor((value - min) / binSize);
            data[index]++;
        }
    }

    void update(std::vector<T> values) {
        for (T value : values) {
            update(value);
        }
    }

    void normalise() {
        T norm = std::accumulate(data.begin(), data.end(), static_cast<T>(0.0));
        std::transform(data.begin(), data.end(), data.begin(), [norm](T v) { return v / norm; });
    }

private:
    uint numBins;
    T min, max;
    T binSize;
    std::vector<uint> data;
};

#endif //IMPURITY_MODEL_HISTOGRAM1D_H
