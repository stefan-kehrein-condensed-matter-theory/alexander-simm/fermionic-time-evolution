#include "Timer.h"
#include <sstream>

using namespace std;
using namespace std::chrono;

Timer::Timer() : nthRound(0) {
}

void Timer::start() {
    nthRound = 1;
    startTime = high_resolution_clock::now();
}

void Timer::nextRound() {
    nthRound++;
    roundStartTime = high_resolution_clock::now();
}

string Timer::toString(uint numTotalRounds) {
    auto now = high_resolution_clock::now();

    stringstream out;
    out << "round time=";
    formatDuration(now - roundStartTime, out);
    out << ", total time=";
    formatDuration(now - startTime, out);
    out << ", overall estimate=";
    formatDuration((now - startTime) * numTotalRounds / nthRound, out);

    return out.str();
}

void Timer::formatDuration(high_resolution_clock::duration duration, stringstream &out) {
    int sec = (int) duration_cast<seconds>(duration).count();
    int min = (int) duration_cast<minutes>(duration).count();
    int h = (int) duration_cast<hours>(duration).count();

    if (h > 1) {
        out << h << "h ";
        min %= 60;
        sec %= 3600;
    }
    if (min > 1) {
        out << min << "m ";
        sec %= 60;
    }
    out << sec << "s";
}