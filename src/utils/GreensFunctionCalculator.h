#ifndef IMPURITY_MODEL_GREENSFUNCTIONCALCULATOR_H
#define IMPURITY_MODEL_GREENSFUNCTIONCALCULATOR_H

#include "../HamiltonianBlock.h"
#include "../BlockDiagonalHamiltonian.h"
#include "../Basis.h"
#include "Timer.h"

class GreensFunctionCalculator {
public:
    /**
     * Creates a calculator for the spectral function, Green's function and density of states of a Hamiltonian.
     * The delta peaks are approximated by Lorentz functions of width eta.
     * @param H block of a Hamiltonian with a fixed particle number
     * @param Hplus block of the Hamiltonian with one more particle
     * @param Hminus block of the Hamiltonian with one less particle
     * @param eta width of the approximated delta peaks
     */
    GreensFunctionCalculator(HamiltonianBlock *H, HamiltonianBlock *Hplus, HamiltonianBlock *Hminus,
                             double eta, double groundStateEnergyTolerance = 1e-10);

    ~GreensFunctionCalculator();

    uint numGroundStates() const;

    /**
     * Calculates the on-site spectral function -1/pi * Im(G^R) in spectral representaton for a specific
     * energy omega.
     */
    double spectralFunction(double omega, uint site) const;

    /**
     * Calculates the on-site spectral function -1/pi * Im(G^R) in spectral representaton for a range of
     * energies omegas.
     */
    arma::vec spectralFunction(arma::vec omegas, uint site) const;

    /**
     * Calculates the density of states (trace of spectral function) for a specific energy omega.
     */
    double densityOfStates(double omega);

    /**
     * Calculates the density of states (trace of spectral function) for a range of energies omegas.
     */
    arma::vec densityOfStates(arma::vec omegas);

private:
    /** Hamiltonians with N, N-1, and N+1 particles */
    HamiltonianBlock *H, *Hplus, *Hminus;

    /** Width of approximated delta peaks */
    double eta;

    double groundStateEnergyTolerance;
    Timer timer;

    /** Creation and annihilation operators for each site. Will be empty if no lower or upper block exists. */
    std::vector<arma::mat *> creationOps, annihilationOps;

    /** Ground state energies and eigenstates (can be degenerate) */
    std::vector<double> gsEnergies;
    std::vector<arma::cx_vec> groundStates;

    /**
     * Computes the matrix that has entries (state coefficients) for all possible conversions of states in Hsrc into
     * states in Hdst by annihilating a particle at the given site.
     * @param src source Hamiltonian with more particles
     * @param dst destination Hamiltonian with less particles
     * @param site the site that should be annihilated
     */
    static arma::mat *annihilationMatrix(const Basis &src, const Basis &dst, uint site);

    /**
     * Computes the matrix that has entries (state coefficients) for all possible conversions of states in HQm into
     * states in HQp by creating a particle at the given site.
     * @param src source Hamiltonian with more particles
     * @param dst destination Hamiltonian with less particles
     * @param site the site that should be annihilated
     * @return
     */
    static arma::mat *creationMatrix(const Basis &src, const Basis &dst, uint site);
};

#endif //IMPURITY_MODEL_GREENSFUNCTIONCALCULATOR_H
