#ifndef IMPURITY_MODEL_STORAGEFILE_H
#define IMPURITY_MODEL_STORAGEFILE_H

#include <string>
#include <vector>
#include <filesystem>
#include <armadillo>

class StorageFile {
public:
    explicit StorageFile(std::filesystem::path filename);

    StorageFile(const StorageFile &s);

    ~StorageFile() = default;

    StorageFile &operator=(const StorageFile &s);

    std::filesystem::path getPath() const;

    bool exists() const;

    void load();

    void save();

    [[nodiscard]] uint numColumns() const;

    void addColumn(const std::vector<double> &column, const std::string &name);

    void addColumn(const arma::vec &column, const std::string &name);

    void removeColumn(uint index);

    void setColumn(uint index, const std::vector<double> &column);

    void setColumn(std::string name, const std::vector<double> &column);

    bool hasColumn(std::string name);

    std::vector<std::string> getColumnNames() const;

    std::vector<std::vector<double>> getColumns() const;

    std::vector<double> getColumn(uint index) const;

    std::vector<double> getColumn(std::string name) const;

    std::string getColumnName(uint index) const;

    void setColumns(const std::vector<std::vector<double>> &cols, const std::vector<std::string> &names);

    void clear();

    void deleteFile();

    static void combineFilesInclude(std::vector<std::pair<std::string, std::vector<uint>>> inputFiles,
                             std::string outputFile);

    static void combineFilesExclude(std::vector<std::pair<std::string, std::vector<uint>>> inputFiles,
                                    std::string outputFile);

private:
    const char DELIMITER = ',';

    std::filesystem::path filename;
    std::vector<std::string> columnNames;
    std::vector<std::vector<double>> columns;

    double toDouble(std::string s) const;
};

#endif //IMPURITY_MODEL_STORAGEFILE_H
