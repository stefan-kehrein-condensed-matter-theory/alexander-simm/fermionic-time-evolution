#include <iostream>
#include "utils.h"
#include "State.h"
#include <stack>

using namespace std;

State::State(uint N, ulong stateNumber) : N(N), coefficient(1), bits(stateNumber) {
}

State::State(const State &s) {
    this->operator=(s);
}

State &State::operator=(const State &other) {
    if (this != &other) {
        N = other.N;
        bits = other.bits;
        coefficient = other.coefficient;
    }

    return *this;
}

bool State::operator==(const State &other) const {
    return (N == other.N) && (bits == other.bits) && (coefficient == other.coefficient);
}

bool State::operator!=(const State &other) const {
    return !((*this) == other);
}

int State::numParticles() const {
    uint sum = 0;
    for (uint i = 0; i < N; i++) {
        if (isSiteOccupied(i))
            sum++;
    }
    return sum;
}

uint State::numOccupiedSites(uint pos1, uint pos2) const {
    const uint start = min(pos1, pos2);
    const uint end = max(pos1, pos2);

    uint sum = 0;
    for (uint i = start; i <= end; ++i) {
        sum += isSiteOccupied(i);
    }
    return sum;
}

uint State::numOccupiedSites(set<uint> sites) const {
    uint sum = 0;
    for (uint i : sites) {
        if (isSiteOccupied(i))
            sum++;
    }
    return sum;
}

bool State::isZero() const {
    return coefficient == 0;
}

int State::getCoefficient() const {
    return coefficient;
}

std::vector<uint> State::getOccupiedSites() const {
    vector<uint> occupied;
    for (uint i = 0; i < N; i++) {
        if (isSiteOccupied(i))
            occupied.push_back(i);
    }
    return occupied;
}

bool State::isSiteOccupied(uint site) const {
    return (bits & (1 << site)) != 0;
}

uint State::numDifferentSites(const State &other) const {
    uint different = 0;
    for (uint i = 0; i < N; i++) {
        if (isSiteOccupied(i) != other.isSiteOccupied(i))
            different++;
    }
    return different;
}

void State::annihilate(uint site) {
    if (coefficient != 0) {
        if (isSiteOccupied(site)) {
            for (uint i = 0; i < site; i++) {
                if (isSiteOccupied(i))
                    coefficient = -coefficient;
            }

            bits ^= (1l << site);
        } else {
            coefficient = 0;
        }
    }
}

void State::create(uint site) {
    if (coefficient != 0) {
        if (!isSiteOccupied(site)) {
            for (uint i = 0; i < site; i++) {
                if (isSiteOccupied(i))
                    coefficient = -coefficient;
            }

            bits ^= (1l << site);
        } else {
            coefficient = 0;
        }
    }
}

State State::applyTerm(uint i, uint j) const {
    State state(*this);
    state.annihilate(j);
    state.create(i);
    return state;
}

void State::applyTerm(uint i, uint j, State &dst) const {
    dst = *this;
    dst.annihilate(j);
    dst.create(i);
}

State State::applyTerm(uint i, uint j, uint k, uint l) const {
    State state(*this);
    state.annihilate(l);
    state.annihilate(k);
    state.create(j);
    state.create(i);
    return state;
}

void State::applyTerm(uint i, uint j, uint k, uint l, State &dst) const {
    dst = *this;
    dst.annihilate(l);
    dst.annihilate(k);
    dst.create(j);
    dst.create(i);
}

string State::toString() const {
    if (coefficient == 0) {
        return "0";
    } else {
        stringstream s;
        s << "(";

        if (coefficient == -1)
            s << "-";

        Utils::printVector(s, getOccupiedSites(), ",", false);
        s << ")";

        return s.str();
    }
}

std::string State::toBinaryString() const {
    stringstream s;
    for (uint i = 0; i < N; i++) {
        s << (isSiteOccupied(i) ? 1 : 0);
    }
    return s.str();
}

ulong State::getStateNumber() const {
    return bits;
}

State State::combineStates(State stateA, std::vector<uint> sitesA, State stateB, std::vector<uint> sitesB) {
    State dst(sitesA.size() + sitesB.size(), 0);

    for (uint i = 0; i < stateA.N; i++) {
        if (stateA.isSiteOccupied(i))
            dst.create(sitesA[i]);
    }
    for (uint i = 0; i < stateB.N; i++) {
        if (stateB.isSiteOccupied(i))
            dst.create(sitesB[i]);
    }

    dst.coefficient = 1;
    return dst;
}