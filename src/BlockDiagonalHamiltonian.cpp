#include "BlockDiagonalHamiltonian.h"
#include "Log.h"

using namespace std;
using namespace arma;

//TODO: use Basis object for everything in here
BlockDiagonalHamiltonian::BlockDiagonalHamiltonian(uint N, vector<HamiltonianTerm *> terms, bool sparse)
        : Hamiltonian(N, terms), basis(N, -1), blocks(0), eigenvalues((uword) 0) {
    for (uint Q = 0; Q <= N; Q++) {
        blocks.push_back(new HamiltonianBlock(N, Q, terms, sparse && Q > 2 && Q < blocks.size() - 2));
    }
}

BlockDiagonalHamiltonian::BlockDiagonalHamiltonian(uint N, vector<HamiltonianTerm *> terms, double impurityEnergy,
                                                   double V, bool sparse)
        : Hamiltonian(N, terms, impurityEnergy, V), basis(N + 1, -1), blocks(0),
          eigenvalues((uword) 0) {
    for (uint Q = 0; Q <= N + 1; Q++) {
        blocks.push_back(new HamiltonianBlock(N, Q, terms, impurityEnergy, V,
                                              sparse && Q > 2 && Q < blocks.size() - 2));
    }
}

BlockDiagonalHamiltonian::BlockDiagonalHamiltonian(const BlockDiagonalHamiltonian &h)
        : Hamiltonian(h), basis(h.basis) {
    this->operator=(h);
}

BlockDiagonalHamiltonian::~BlockDiagonalHamiltonian() {
    // delete all blocks
    for (int i = 0; i < blocks.size(); i++) {
        if (blocks[i] != nullptr) {
            delete blocks[i];
            blocks[i] = nullptr;
        }
    }
}

BlockDiagonalHamiltonian &BlockDiagonalHamiltonian::operator=(const BlockDiagonalHamiltonian &h) {
    if (this != &h) {
        Hamiltonian::operator=(h);
        basis = h.basis;
        blocks = h.blocks;
        eigenvalues = h.eigenvalues;
        eigenvectors = h.eigenvectors;
    }

    return *this;
}

//TODO: cache instead of copying every time
cx_mat BlockDiagonalHamiltonian::asMatrix() const {
    cx_mat H(basis.dim(), basis.dim());
    H.zeros();

    uint off = 0;

    for (const HamiltonianBlock *block : blocks) {
        uint dim = block->getBasis().dim();
        H.submat(off, off, off + dim - 1, off + dim - 1) = block->matrix();
        off += dim;
    }

    return H;
}

arma::sp_cx_mat BlockDiagonalHamiltonian::asSparseMatrix() const {
    return sp_cx_mat(asMatrix());
}

const Basis &BlockDiagonalHamiltonian::getBasis() const {
    return basis;
}

bool BlockDiagonalHamiltonian::isUsingSparseMatrix() const {
    return false;
}

double BlockDiagonalHamiltonian::getSparseness() const {
    double count = 0;
    for (HamiltonianBlock *block : blocks) {
        uint d = block->getBasis().dim();
        count += block->getSparseness() * (d * d);
    }
    return count / (basis.dim() * basis.dim());
}

bool BlockDiagonalHamiltonian::isDiagonalised() const {
    return !eigenvalues.empty();
}

void BlockDiagonalHamiltonian::diagonalise(bool includeEigenvectors, bool onlyGroundState) {
    eigenvalues.resize(basis.dim());
    if (includeEigenvectors) {
        eigenvectors.resize(basis.dim(), onlyGroundState ? 1 : basis.dim());
        eigenvectors.zeros();
    }

    uint off = 0;
    double lowestEnergy = numeric_limits<double>::infinity();

    for (HamiltonianBlock *block : blocks) {
        Log::debug() << "diagonalising block " << block->getBasis().getQ() << endl;
        block->diagonalise(includeEigenvectors, onlyGroundState);
        uint dim = block->getBasis().dim();

        if (onlyGroundState) {
            if (block->getEigenvalues()[0] < lowestEnergy) {
                //TODO: this is maximally inefficient! improve it!
                eigenvalues[0] = block->getEigenvalues()[0];
                eigenvectors.fill(0);
                eigenvectors.submat(off, 0, off + dim - 1, 0) = block->getEigenvectors().col(0);
                lowestEnergy = eigenvalues[0];
            }
        } else {
            // copy eigenvalues
            eigenvalues.subvec(off, off + dim - 1) = block->getEigenvalues();

            // copy eigenvectors
            if (includeEigenvectors) {
                eigenvectors.submat(off, off, off + dim - 1, off + dim - 1) = block->getEigenvectors();
            }
        }

        off += dim;
    }
}

const vec &BlockDiagonalHamiltonian::getEigenvalues() const {
    if (!isDiagonalised())
        throw runtime_error("Hamiltonian is not diagonalised");
    return eigenvalues;
}

const cx_mat &BlockDiagonalHamiltonian::getEigenvectors() const {
    if (!isDiagonalised())
        throw runtime_error("Hamiltonian is not diagonalised");
    return eigenvectors;
}

vector<HamiltonianBlock *> BlockDiagonalHamiltonian::getBlocks() const {
    return blocks;
}

HamiltonianBlock *BlockDiagonalHamiltonian::getBlock(uint Q) const {
    for (HamiltonianBlock *block: blocks) {
        if (block != nullptr && block->getBasis().getQ() == Q)
            return block;
    }
    return nullptr;
}

void BlockDiagonalHamiltonian::updateImpurityEnergy(double E) {
    for (HamiltonianBlock *block : blocks) {
        block->updateImpurityEnergy(E);
    }
    impurityEnergy = E;

    eigenvalues.clear();
    eigenvectors.clear();
}

void BlockDiagonalHamiltonian::updateHoppingPotential(double V) {
    for (HamiltonianBlock *block : blocks) {
        block->updateHoppingPotential(V);
    }
    impurityHopping = V;

    eigenvalues.clear();
    eigenvectors.clear();
}

void BlockDiagonalHamiltonian::updateMatrix(bool updateTerms) {
    // terms need to be updated for all blocks at once
    if(updateTerms) {
        for(HamiltonianTerm *term : terms) {
            term->update();
        }
    }

    // update every block without updating the terms in each block
    for(HamiltonianBlock *block : blocks) {
        block->updateMatrix(false);
    }
}
