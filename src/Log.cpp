#include "Log.h"
#include <iostream>

using namespace std;

Log::Log() : nullStream(&nullBuffer) {
    logLevel = Level::INFO;
}

Log &Log::getInstance() {
    static Log instance;
    return instance;
}

Log::Level Log::getLevel() const {
    return logLevel;
}

void Log::setLevel(Level l) {
    logLevel = l;
}

std::ostream &Log::log(Level level) {
    if (level <= logLevel) {
        return cout;
    } else {
        return nullStream;
    }
}

std::ostream &Log::info() {
    return Log::getInstance().log(Level::INFO);
}

std::ostream &Log::debug() {
    return Log::getInstance().log(Level::DEBUG);
}
