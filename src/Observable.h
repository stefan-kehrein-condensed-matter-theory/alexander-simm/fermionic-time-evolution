#ifndef IMPURITY_MODEL_OBSERVABLE_H
#define IMPURITY_MODEL_OBSERVABLE_H

#include <armadillo>

class Observable {
public:
    virtual void measure(const arma::cx_vec &psi, uint step, uint numSteps) = 0;
};

#endif //IMPURITY_MODEL_OBSERVABLE_H
